# vim: tabstop=4 noexpandtab shiftwidth=4
import logging
import shutil
import os
import errno
import sys
import re
import signal

logging.basicConfig(level=logging.INFO)

oldargv = sys.argv
sys.argv = [oldargv[0], "-b"]
logging.info("importing ROOT")
import ROOT
ROOT.gROOT
logging.info("OK")

import atexit
@atexit.register
def quite_exit():
    ROOT.gSystem.Exit(0)

# lets you interrupt the job by ctrl+C immediately
signal.signal(signal.SIGINT, signal.SIG_DFL)

logging.info("loading packages")
ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")
sys.argv = oldargv


logging.info("creating new sample handler")
sh_all = ROOT.SH.SampleHandler()

####################################
# change path to input sample here:
####################################
search_directories = []

basedir = "truth_pileup/"

#input_path = os.path.dirname(os.environ["ASG_TEST_FILE_MC"])
#input_path = "/tmp/mansour/truth_pileup/mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.merge.HITS.e3581_s2578_s2195"
#input_path = "/tmp/mansour/truth_pileup/mc15_13TeV.119995.Pythia8_A2MSTW2008LO_minbias_inelastic_low.recon.AOD.e3432_s2081_s2132_r6125"
#input_path = "/tmp/mansour/truth_pileup/valid3.147407.PowhegPythia8_AZNLO_Zmumu.recon.AOD.e3099_s2578_r6939_tid06327014_00"
input_path = "/afs/cern.ch/work/b/bgui/AOD_Files/valid3.147407.PowhegPythia8_AZNLO_Zmumu.recon.AOD.e3099_s2578_r6939_tid06327014_00/"
#input_path = "/tmp/mansour/truth_pileup/valid1.110401.PowhegPythia_P2012_ttbar_nonallhad.recon.AOD.e3099_s2578_r7052"
#input_path = "/afs/cern.ch/work/j/jmansour/XAOD.pileupTruth.ttbar.pool.root"
#input_path = "/tmp/mansour/samples/XAOD.pileupTruth.ttbar.pool.root"

if input_path:
    if os.path.isdir(input_path):
        input_dir = input_path
        pattern = "*.root"
        pattern = "*.root.1"
        #  if "*" in input_dir or "?" in input_dir:
            #  print "Error, wildcards not supported in --input directory (only in filename)"
            #  sys.exit(1)
    else:
        input_dir, pattern = os.path.split(input_path)

    logging.info("input dir: %s", input_dir)
    logging.info("pattern: %s", pattern)
    search_directories.append(input_dir)




#search_directories = ("/ngse5/atlas/user/fmeloni/InDet_xAOD/",)
#search_directories = ("/ngse5/atlas/user/fmeloni/mc15_PU_xAOD/",)
#search_directories = ("/tmp/jmansour/data15_13TeV.00266904.express_express.merge.AOD.x339_m1435/",)
#search_directories = ("/tmp/jmansour/data15_13TeV.00266919.physics_MinBias.merge.AOD.f594_m1435/",)
#search_directories = ("/tmp/jmansour/data15_13TeV.00267223.physics_MinBias.merge.AOD.x340_m1435/",)
#search_directories = ("/tmp/jmansour/mc15_13TeV.159000.ParticleGenerator_nu_E50.recon.AOD.e3711_s2576_s2132_r6220_tid05293002_00/",)
# scan for datasets in the given directories
short_name = "default_set"
for directory in search_directories:
    ROOT.SH.ScanDir().filePattern(pattern).scan(sh_all, directory)

# set the name of the tree in our files
sh_all.setMetaString("nc_tree", "CollectionTree")

sh_all.Print()
for sample in sh_all:
    logging.info("%s: %d files" % (sample.name(), sample.numFiles()))
    for fn in sample.makeFileList():
        logging.info("  "+fn)

# print out the samples we found
logging.info("%d different datasets found scanning all directories", len(sh_all))

# this is the basic description of our job
logging.info("creating new job")
job = ROOT.EL.Job()
job.sampleHandler(sh_all)
  
# if options.nevents:
#     logging.info("processing only %d events", options.nevents)
#     job.options().setDouble(ROOT.EL.Job.optMaxEvents, options.nevents)
#  job.options().setDouble(ROOT.EL.Job.optMaxEvents, 10)

# if options.skip_events:
#     logging.info("skipping first %d events", options.skip_events)
#     job.options().setDouble(ROOT.EL.Job.optSkipEvents, options.skip_events)


#Set up the job for xAOD access:
ROOT.xAOD.Init().ignore();

# add our algorithm to the job
logging.info("adding algorithms")


#  method = "compare_matching"
#  method = "compare_methods"
method = "compare_methods_cut"
#  method = "compare_tracksel"


if method == "compare_matching":
    # code for comparing different truth-reco vertex matching
    outdir = "compare_matching"
    alg = ROOT.TVATester()
    alg.description = "Smart matching"
    alg.directoryName = "smart"
    alg.matchMethod = "smart"
    alg.associationMethod = "Tight"
    job.algsAdd(alg)

    alg = ROOT.TVATester()
    alg.description = "Nearest matching"
    alg.directoryName = "nearest"
    alg.matchMethod = "nearest"
    alg.associationMethod = "Tight"
    job.algsAdd(alg)

elif method == "compare_methods":
    # code for comparing different methods:
    outdir = "compare_methods"
    alg = ROOT.TVATester()
    alg.description = "Tight association method"
    alg.directoryName = "tightassoc"
    alg.associationMethod = "Tight"
    alg.matchMethod = "smart"
    alg.doTrackCut = True
    alg.trackCutLevel = "TightPrimary"
    alg.trackMinPt = 400.
    job.algsAdd(alg)

    alg = ROOT.TVATester()
    alg.description = "Loose association method"
    alg.directoryName = "looseassoc"
    alg.associationMethod = "Loose"
    alg.matchMethod = "smart"
    alg.doTrackCut = True
    alg.trackCutLevel = "TightPrimary"
    alg.trackMinPt = 400.
    job.algsAdd(alg)

    alg = ROOT.TVATester()
    alg.description = "Electron association method"
    alg.directoryName = "electronassoc"
    alg.associationMethod = "Electron"
    alg.matchMethod = "smart"
    alg.doTrackCut = True
    alg.trackCutLevel = "TightPrimary"
    alg.trackMinPt = 400.
    job.algsAdd(alg)

    alg = ROOT.TVATester()
    alg.description = "Muon association method"
    alg.directoryName = "muonassoc"
    alg.associationMethod = "Muon"
    alg.matchMethod = "smart"
    alg.doTrackCut = True
    alg.trackCutLevel = "TightPrimary"
    alg.trackMinPt = 400.
    job.algsAdd(alg)

elif method == "compare_methods_cut":
    # code for comparing different methods, with particle type cut:
    outdir = "compare_methods_cut"
    alg = ROOT.TVATester()
    alg.description = "Tight association method"
    alg.directoryName = "tightassoc"
    alg.associationMethod = "Tight"
    alg.matchMethod = "smart"
    alg.doTrackCut = True
    alg.trackCutLevel = "TightPrimary"
    alg.trackMinPt = 400.
    alg.trackAbsPdgId = 211
    job.algsAdd(alg)

    alg = ROOT.TVATester()
    alg.description = "Loose association method"
    alg.directoryName = "looseassoc"
    alg.associationMethod = "Loose"
    alg.matchMethod = "smart"
    alg.doTrackCut = True
    alg.trackCutLevel = "TightPrimary"
    alg.trackMinPt = 400.
    alg.trackAbsPdgId = 211
    job.algsAdd(alg)

    alg = ROOT.TVATester()
    alg.description = "Electron association method"
    alg.directoryName = "electronassoc"
    alg.associationMethod = "Electron"
    alg.matchMethod = "smart"
    alg.doTrackCut = True
    alg.trackCutLevel = "TightPrimary"
    alg.trackMinPt = 400.
    alg.trackAbsPdgId = 11
    job.algsAdd(alg)

    alg = ROOT.TVATester()
    alg.description = "Muon association method"
    alg.directoryName = "muonassoc"
    alg.associationMethod = "Muon"
    alg.matchMethod = "smart"
    alg.doTrackCut = True
    alg.trackCutLevel = "TightPrimary"
    alg.trackMinPt = 400.
    alg.trackAbsPdgId = 13
    job.algsAdd(alg)

elif method == "compare_tracksel":
    # code for comparing different track selections
    outdir = "compare_tracksel"
    alg = ROOT.TVATester()
    alg.description = "Tight tracks"
    alg.associationMethod = "Tight"
    alg.directoryName = "tight"
    alg.doTrackCut = True
    alg.trackCutLevel = "TightPrimary"
    alg.trackMinPt = 400.
    job.algsAdd(alg)

    alg = ROOT.TVATester()
    alg.description = "Loose tracks"
    alg.associationMethod = "Tight"
    alg.directoryName = "loose"
    alg.doTrackCut = True
    alg.trackCutLevel = "Loose"
    alg.trackMinPt = 400.
    job.algsAdd(alg)

    alg = ROOT.TVATester()
    alg.description = "No track cut"
    alg.associationMethod = "Tight"
    alg.directoryName = "nocut"
    alg.doTrackCut = False
    job.algsAdd(alg)

else:
    log.error("Unknown method %s", method)
    sys.exit(1)

# # this doesnt work, we could do something like that in Athena:
# alg.m_trackSel = ROOT.InDet.InDetTrackSelectionTool("LooseTrackSelection");
# alg.m_trackSel.setProperty("CutLevel", "Loose")
# alg.m_trackSel.setProperty("minPt", 400.)
# alg.m_trackSel.initialize()
# print dir(alg.m_trackSel)

# make the driver we want to use
# this one works by running the algorithm directly
logging.info("creating driver")

logging.info("running on direct")
driver = ROOT.EL.DirectDriver()
logging.info("submit job")

realOutDir = basedir + outdir

if os.path.exists(realOutDir):
    logging.error("output path %s already exists", realOutDir)
    sys.exit(1)

logging.info("will create output directory %s", realOutDir)
#  while True:
#      logging.info("is this OK? [y/n]")
#      inp = raw_input().lower()
#      if inp == "y":
#          break
#      elif inp == "n":
#          logging.info("cancelling")
#          sys.exit(0)

try:
    os.makedirs(os.path.dirname(realOutDir))
except OSError:
    pass

driver.submit(job, realOutDir)
logging.info("the output dir is %s", realOutDir)
