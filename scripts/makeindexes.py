
from __future__ import print_function
from collections import OrderedDict
import glob
import jinja2
import os
import struct
import time

TEMPLATE=u"""
<html>
<head>
<style>
body {
  font-family: Arial, Helvetica;
  background-color: #fafaf5;
}
.plot {
  display: inline-block;
  border: 1px solid #ccc;
  background-color: #fff;
  margin: 0.5em;
  padding: 0.5em;
  // box-shadow: 1px 1px 2px #888;
}
.plot h3 {
  margin: 0px;
  text-align: center;
}
.plot img {
  max-width: 600px;
}
table.fields td {
  border: 1px solid #ccc;
  padding: 0.2em 0.4em;
}
table.fields {
  border-collapse: collapse;
}

a.anchorlink {
  float: right;
  text-decoration: none;
}

div.plot:not(:hover) a.anchorlink {
  color: #ddd;
}

.overlay {
  border-radius: 2px;
  background-color: #F3F2EF;
  border: 1px solid #DEDAD3;
  color: #222;
  padding: 8px 12px;
  position: fixed;
  right: 0px;
  top:  0px;
  margin:  10px;
  max-width: 20em;
  // box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
}
.overlay input {
  font-size: 100%;
  margin: 0 0.3em;
}
</style>
</head>
<body>
<div class="overlay">
    <div style="text-align: right">
    <label for="filter">Filter:</label>
    <input type="text" id="filter"/sbin>
    <a href="#" title="Show/hide help" onclick="filterHelp.style.display = filterHelp.style.display ? '' : 'none'">&nbsp;?&nbsp;</a>
    </div>
    <div id="filterHelp" style="display: none">
    <p>Enter multiple search terms to filter plots.  The search is performed on the histogram name, case insensitive.  Prefix a search term with "-" to exclude.  You can use "^" and "$" to specify the beginning or end of the histogram name.</p>
    <p>Examples:</p>
    <dl>
        <dt>y_tt</dt>
        <dd>Finds y_tt and dy_tt</dd>
        <dt>^y_tt</dt>
        <dd>Finds y_tt, but not dy_tt</dd>
        <dt>et 2b</dt>
        <dd>Finds ETmiss, ETmiss_2l_2b, ...</dd>
    </dl>
    </div>
</div>

  {% if common_fields %}
    <table class="fields">
      {% for key, value in common_fields.items() %}
        <tr>
        <td>{{ key }}</td><td>{{ value }}</td>
        </tr>
      {% endfor %}
    </table>
  {% endif %}

  {% for image in images %}
    <div class="plot">
      {% if image.fields.variable %}
        <a name="{{ image.fields.variable }}">
          <h3>{{ image.fields.variable }}
          <a href="#{{ image.fields.variable }}" class="anchorlink" title="Link to this plot">&sect;</a></h3>
        </a>
      {% endif %}
      <img src="{{ image.filename }}" title="{{ image.fields_str }}"/>
      <table class="fields">
        {% for key, value in image.special_fields.items() %}
          <tr>
          <td>{{ key }}</td><td>{{ value }}</td>
          </tr>
        {% endfor %}
      </table>
    </div>
  {% endfor %}

<div id="noResults" style="display: none">No plots match the given filter.</div>

<script type="text/javascript">
    var index = {};
    var divs = document.querySelectorAll("div.plot");
    // var divs = document.querySelectorAll("div>div");
    for (var i = 0; i < divs.length; i++) {
        var div = divs[i];
        var img = div.querySelector("img");
        if (!img) continue;
        var parts = img.src.split("/");
        var filename = parts[parts.length-1];
        var name = filename.split(".")[0];
        index[name] = div;
    }


function filter(searchstring) {
    var searchparts = searchstring.toLowerCase().split(" ");
    var somethingVisible = false;

    for (var k in index) {
        var key = "^" + k.toLowerCase() + "$"
        var visible = true;
        for (var i in searchparts) {
            var part = searchparts[i];
            var show;
            if (part[0] == "-") {
                if (part.length == 1) {
                    // ignore plain "-", the user is still typing
                    show = true;
                } else {
                    show = (key.indexOf(part.substring(1)) == -1);
                }
            } else {
                show = (key.indexOf(part) > -1);
            }
            visible = visible & show;
        }
        var div = index[k];
        div.style.display = visible ? "" : "none";
        somethingVisible = somethingVisible | visible;
    }

    var noResults = document.getElementById("noResults");
    noResults.style.display = somethingVisible ? "none" : "";
}

var filterInput = document.getElementById("filter");
filterInput.oninput = function(event) {
    filter(event.target.value);
}

</script>


</body>
</html>
"""


class Image(object):
    def __init__(self, filename, fields):
        self.filename = filename
        self.fields = fields
        self.special_fields = {}

    @property
    def fields_str(self):
        return "\n".join(k+": "+v for k,v in self.fields.iteritems())


def main():
    for path, dirs, files in os.walk("plots"):
        print(path, dirs, files)

        fnames = [f for f in files if f.endswith(".png")]
        if not fnames:
            continue

        images = []
        for png in fnames:
            with open(os.path.join(path, png), 'rb') as fobj:
                data = fobj.read()

            fields = OrderedDict()

            # check signature
            assert data[:8] == b'\x89\x50\x4E\x47\x0D\x0A\x1A\x0A'

            for chunk_type, chunk_data in chunk_iter(data):
                if chunk_type == b'tEXt':
                    key, sep, val = chunk_data.decode('iso-8859-1').partition("\0")
                    fields[key] = val

            images.append(Image(png, fields))

        if images:
            all_keys = set()
            for image in images:
                for key in image.fields:
                    all_keys.add(key)

            common_fields = OrderedDict()
            for key in all_keys:
                # is this a common field?
                if key == "plotCreated":
                    # for creation time, use the earliest time
                    mintime = None
                    for image in images:
                        timestr = image.fields.get(key, "")
                        imgtime = time.strptime(timestr, "%c %Z")
                        if mintime is None or imgtime < mintime:
                            mintime = imgtime

                    common_fields[key] = time.strftime("%c %Z", mintime)
                    continue

                else:
                    # for other fields, merge if the same on all plots
                    firstval = images[0].fields.get(key, None)
                    if all(image.fields.get(key, None) == firstval
                            for image in images[1:]):
                        common_fields[key] = firstval
                        continue

                # leave as individual field
                for img in images:
                    if key in img.fields:
                        img.special_fields[key] = img.fields[key]

        with open(os.path.join(path, "index.html"), "w") as f:
            tmpl = jinja2.Template(TEMPLATE)
            f.write(tmpl.render(images=images, common_fields=common_fields))


# chunk generator
def chunk_iter(data):
    """ Reads PNG data and returns chunks.  From http://blender.stackexchange.com/a/35527 """
    total_length = len(data)
    end = 4

    while(end + 8 < total_length):
        # length = int.from_bytes(data[end + 4: end + 8], 'big')
        length = struct.unpack(">I", data[end + 4: end + 8])[0]
        begin_chunk_type = end + 8
        begin_chunk_data = begin_chunk_type + 4
        end = begin_chunk_data + length

        yield (data[begin_chunk_type: begin_chunk_data],
               data[begin_chunk_data: end])


if __name__ == "__main__":
    main()

