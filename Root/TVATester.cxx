#include "TVATest/TVATester.h"
#include "TVATest/TMetaRecord.h"

// Packages
#include "TrackVertexAssociationTool/LooseTrackVertexAssociationTool.h"
#include "TrackVertexAssociationTool/TightTrackVertexAssociationTool.h"
#include "TrackVertexAssociationTool/BaseTrackVertexAssociationTool.h"
#include "TrackVertexAssociationTool/ElectronTrackVertexAssociationTool.h"
#include "TrackVertexAssociationTool/MuonTrackVertexAssociationTool.h"
#include "TrackVertexAssociationTool/CheckExc.h"
#include "InDetTruthVertexValidation/InDetVertexTruthMatchUtils.h"

// EventLoop
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

// xAOD
#include <xAODEventInfo/EventInfo.h>
#include <xAODTracking/TrackParticleContainer.h>
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include <xAODTracking/VertexContainer.h>
#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthPileupEventContainer.h"
#include <xAODTruth/TruthParticle.h>
#include <xAODTruth/TruthVertex.h>
#include <xAODTruth/xAODTruthHelpers.h>

// ROOT
#include "TDatabasePDG.h"
#include "TFile.h"
#include "TH1F.h"

#include <sstream>

// this is needed to distribute the algorithm to the workers
ClassImp(TVATester)


static const char* toString(xAOD::VxType::VertexType type);
static const char* toString(InDetVertexTruthMatchUtils::VertexMatchType type);

static const char* getParticleName(int pdgId) {
  static TDatabasePDG pdg;
  if (pdgId == 990) {
    return "pomeron";
  }

  const auto *particle = pdg.GetParticle(pdgId);
  if (particle) {
    return particle->GetName();
  } else {
    return "unknown";
  }
}

TVATester::TVATester(): associationMethod(""), directoryName(""), description(""), doTrackCut(false), trackCutLevel("TightPrimary"), trackMinPt(-1), trackAbsPdgId(-1), matchMethod("smart"), m_trackSel(0)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  Info("TVATester", "---");
}

EL::StatusCode TVATester::setupJob(EL::Job &job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  Info("setupJob", "---");
  job.useXAOD();

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TVATester::histInitialize()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  Info("histInitialize", "---");

  TString d{""};
  if (directoryName != "") {
    d = directoryName.c_str() + TString("/");
  }

  m_hPtT = new TH1F(d+"hPtT", "Track p_{T} (truth assoc); Track p_{T} [GeV];Pairs", 100, 0, 50);
  m_hPtNoT = new TH1F(d+"hPtNoT", "Track p_{T} (no truth assoc); Track p_{T} [GeV];Pairs", 100, 0, 50);
  m_hPtNoTR = new TH1F(d+"hPtNoTR", "Track p_{T} (no truth, but reco assoc); Track p_{T} [GeV];Pairs", 100, 0, 50);
  m_hPtTR = new TH1F(d+"hPtTR", "Track p_{T} (reco assoc); Track p_{T} [GeV];Pairs", 100, 0, 50);
  m_hPtR = new TH1F(d+"hPtR", "Track p_{T} (reco assoc); Track p_{T} [GeV];Pairs", 100, 0, 50);

  // const UInt_t nptBins = 9;
  // const Float_t ptBins[nptBins+1] = { 0.1, 0.5, 1, 2, 3, 5, 7, 10, 14, 20};
  std::vector<Float_t> ptBins;
  for (float x = 0; x < 5; x += 0.5) ptBins.push_back(x);
  for (float x = 5; x < 10; x += 1) ptBins.push_back(x);
  for (float x = 10; x < 50; x += 2) ptBins.push_back(x);
  ptBins.push_back(50);
  UInt_t nptBins = ptBins.size()-1;

  m_hPtVariableT = new TH1F(d+"hPtVariableT", "Track p_{T} (truth assoc); Track p_{T} [GeV];Pairs", nptBins, &ptBins[0]);
  m_hPtVariableNoT = new TH1F(d+"hPtVariableNoT", "Track p_{T} (no truth assoc); Track p_{T} [GeV];Pairs", nptBins, &ptBins[0]);
  m_hPtVariableNoTR = new TH1F(d+"hPtVariableNoTR", "Track p_{T} (no truth, but reco assoc); Track p_{T} [GeV];Pairs", nptBins, &ptBins[0]);
  m_hPtVariableTR = new TH1F(d+"hPtVariableTR", "Track p_{T} (truth&reco assoc); Track p_{T} [GeV];Pairs", nptBins, &ptBins[0]);
  m_hPtVariableR = new TH1F(d+"hPtVariableR", "Track p_{T} (reco assoc); Track p_{T} [GeV];Pairs", nptBins, &ptBins[0]);

  //m_hphiT = new TH1F(d+"hphiT", "Track phi (truth assoc); Track phi", 20, 0, TMath::Pi());
  //m_hphiNoT = new TH1F(d+"hphiNoT", "Track phi (no truth assoc); Track phi", 20, 0, TMath::Pi());
  //m_hphiNoTR = new TH1F(d+"hphiNoTR", "Track phi (no truth, but reco assoc); Track phi", 20, 0, TMath::Pi());
  //m_hphiTR = new TH1F(d+"hphiTR", "Track phi (truth&reco assoc); Track phi", 20, 0, TMath::Pi());
  //m_hphiR = new TH1F(d+"hphiR", "Track phi (reco assoc); Track phi", 20, 0, TMath::Pi());
  m_hphiT = new TH1F(d+"hphiT", "Track phi (truth assoc); Track phi;Pairs", 100, 0, TMath::Pi());
  m_hphiNoT = new TH1F(d+"hphiNoT", "Track phi (no truth assoc); Track phi;Pairs", 100, 0, TMath::Pi());
  m_hphiNoTR = new TH1F(d+"hphiNoTR", "Track phi (no truth, but reco assoc); Track phi;Pairs", 100, 0, TMath::Pi());
  m_hphiTR = new TH1F(d+"hphiTR", "Track phi (truth&reco assoc); Track phi;Pairs", 100, 0, TMath::Pi());
  m_hphiR = new TH1F(d+"hphiR", "Track phi (reco assoc); Track phi;Pairs", 100, 0, TMath::Pi());

  //m_hetaT = new TH1F(d+"hetaT", "Track eta (truth assoc); Track eta", 30, -3, 3);
  //m_hetaNoT = new TH1F(d+"hetaNoT", "Track eta (no truth assoc); Track eta", 30, -3, 3);
  //m_hetaNoTR = new TH1F(d+"hetaNoTR", "Track eta (no truth, but reco assoc); Track eta", 30, -3, 3);
  //m_hetaTR = new TH1F(d+"hetaTR", "Track eta (truth&reco assoc); Track eta", 30, -3, 3);
  //m_hetaR = new TH1F(d+"hetaR", "Track eta (reco assoc); Track eta", 30, -3, 3);
  m_hetaT = new TH1F(d+"hetaT", "Track eta (truth assoc); Track eta;Pairs", 100, -3, 3);
  m_hetaNoT = new TH1F(d+"hetaNoT", "Track eta (no truth assoc); Track eta;Pairs", 100, -3, 3);
  m_hetaNoTR = new TH1F(d+"hetaNoTR", "Track eta (no truth, but reco assoc); Track eta;Pairs", 100, -3, 3);
  m_hetaTR = new TH1F(d+"hetaTR", "Track eta (truth&reco assoc); Track eta;Pairs", 100, -3, 3);
  m_hetaR = new TH1F(d+"hetaR", "Track eta (reco assoc); Track eta;Pairs", 100, -3, 3);

  std::vector<Float_t> eBins;
  for (float x = 0; x < 5; x += 0.25) eBins.push_back(x);
  for (float x = 5; x < 10; x += 0.5) eBins.push_back(x);
  for (float x = 10; x < 20; x += 1) eBins.push_back(x);
  eBins.push_back(20);
  UInt_t neBins = eBins.size()-1;

  //m_heT = new TH1F(d+"heT", "Track energy (truth assoc); Track energy;Pairs", 300, 250, 1750);
  //m_heNoT = new TH1F(d+"heNoT", "Track energy (no truth assoc); Track energy;Pairs", 300, 250, 1750);
  //m_heNoTR = new TH1F(d+"heNoTR", "Track energy (no truth, but reco assoc); Track energy;Pairs", 300, 250, 1750);
  //m_heTR = new TH1F(d+"heTR", "Track energy (truth&reco assoc); Track energy;Pairs", 300, 250, 1750);
  //m_heR = new TH1F(d+"heR", "Track energy (reco assoc); Track energy;Pairs", 300, 250, 1750);
  m_heT = new TH1F(d+"heT", "Track energy (truth assoc); Track energy;Pairs", neBins, &eBins[0]);
  m_heNoT = new TH1F(d+"heNoT", "Track energy (no truth assoc); Track energy;Pairs", neBins, &eBins[0]);
  m_heNoTR = new TH1F(d+"heNoTR", "Track energy (no truth, but reco assoc); Track energy;Pairs", neBins, &eBins[0]);
  m_heTR = new TH1F(d+"heTR", "Track energy (truth&reco assoc); Track energy;Pairs", neBins, &eBins[0]);
  m_heR = new TH1F(d+"heR", "Track energy (reco assoc); Track energy;Pairs", neBins, &eBins[0]);

  m_hmT = new TH1F(d+"hmT", "Track mass (truth assoc); Track mass;Pairs", 100, 115, 165);
  m_hmNoT = new TH1F(d+"hmNoT", "Track mass (no truth assoc); Track mass;Pairs", 100, 115, 165);
  m_hmNoTR = new TH1F(d+"hmNoTR", "Track mass (no truth, but reco assoc); Track mass;Pairs", 100, 115, 165);
  m_hmTR = new TH1F(d+"hmTR", "Track mass (truth&reco assoc); Track mass;Pairs", 100, 115, 165);
  m_hmR = new TH1F(d+"hmR", "Track mass (reco assoc); Track mass;Pairs", 100, 115, 165);

  m_hd0T = new TH1F(d+"hd0T", "d_{0} (truth assoc);d_{0};Pairs", 100, -10, 10);
  m_hd0NoT = new TH1F(d+"hd0NoT", "d_{0} (no truth assoc);d_{0};Pairs", 100, -10, 10);
  m_hd0NoTR = new TH1F(d+"hd0NoTR", "d_{0} (no truth, but reco assoc);d_{0};Pairs", 100, -10, 10);
  m_hd0TR = new TH1F(d+"hd0TR", "d_{0} (truth&reco assoc);d_{0};Pairs", 100, -10, 10);
  m_hd0R = new TH1F(d+"hd0R", "d_{0} (reco assoc);d_{0};Pairs", 100, -10, 10);
  m_hd0sigT = new TH1F(d+"hd0sigT", "d_{0} significance (truth assoc); d_{0} significance;Pairs", 100, -10, 10);
  m_hd0sigNoT = new TH1F(d+"hd0sigNoT", "d_{0} significance (no truth assoc); d_{0} significance;Pairs", 100, -10, 10);
  m_hd0sigNoTR = new TH1F(d+"hd0sigNoTR", "d_{0} significance (no truth, but reco assoc); d_{0} significance;Pairs", 100, -10, 10);
  m_hd0sigTR = new TH1F(d+"hd0sigTR", "d_{0} significance (truth&reco assoc); d_{0} significance;Pairs", 100, -10, 10);
  m_hd0sigR = new TH1F(d+"hd0sigR", "d_{0} significance (reco assoc); d_{0} significance;Pairs", 100, -10, 10);

  m_hdzSinThetaT = new TH1F(d+"hdzSinThetaT", "#Deltaz sin #theta (truth assoc);#Deltaz sin #theta [mm];Pairs", 100, -5, 5);
  m_hdzSinThetaNoT = new TH1F(d+"hdzSinThetaNoT", "#Deltaz sin #theta (no truth assoc);#Deltaz sin #theta [mm];Pairs", 100, -5, 5);
  m_hdzSinThetaNoTR = new TH1F(d+"hdzSinThetaNoTR", "#Deltaz sin #theta (no truth, but reco assoc);#Deltaz sin #theta [mm];Pairs", 100, -5, 5);
  m_hdzSinThetaTR = new TH1F(d+"hdzSinThetaTR", "#Deltaz sin #theta (truth&reco assoc);#Deltaz sin #theta [mm];Pairs", 100, -5, 5);
  m_hdzSinThetaR = new TH1F(d+"hdzSinThetaR", "#Deltaz sin #theta (reco assoc);#Deltaz sin #theta [mm];Pairs", 100, -5, 5);

  m_hchi2T = new TH1F(d+"hchi2T", ";Track #chi^{2};Pairs", 100, 0, 80);
  m_hchi2NoT = new TH1F(d+"hchi2NoT", ";Track #chi^{2};Pairs", 100, 0, 80);
  m_hchi2NoTR = new TH1F(d+"hchi2NoTR", ";Track #chi^{2};Pairs", 100, 0, 80);
  m_hchi2TR = new TH1F(d+"hchi2TR", ";Track #chi^{2};Pairs", 100, 0, 80);
  m_hchi2R = new TH1F(d+"hchi2R", ";Track #chi^{2};Pairs", 100, 0, 80);
  m_hchi2NDFT = new TH1F(d+"hchi2NDFT", ";Track #chi^{2}/NDF;Pairs", 100, 0, 10);
  m_hchi2NDFNoT = new TH1F(d+"hchi2NDFNoT", ";Track #chi^{2}/NDF;Pairs", 100, 0, 10);
  m_hchi2NDFNoTR = new TH1F(d+"hchi2NDFNoTR", ";Track #chi^{2}/NDF;Pairs", 100, 0, 10);
  m_hchi2NDFTR = new TH1F(d+"hchi2NDFTR", ";Track #chi^{2}/NDF;Pairs", 100, 0, 10);
  m_hchi2NDFR = new TH1F(d+"hchi2NDFR", ";Track #chi^{2}/NDF;Pairs", 100, 0, 10);
  m_hnTRTHitsT = new TH1F(d+"hnTRTHitsT", ";N TRT hits;Pairs", 100, 0, 100);
  m_hnTRTHitsNoT = new TH1F(d+"hnTRTHitsNoT", ";N TRT hits;Pairs", 100, 0, 100);
  m_hnTRTHitsNoTR = new TH1F(d+"hnTRTHitsNoTR", ";N TRT hits;Pairs", 100, 0, 100);
  m_hnTRTHitsTR = new TH1F(d+"hnTRTHitsTR", ";N TRT hits;Pairs", 100, 0, 100);
  m_hnTRTHitsR = new TH1F(d+"hnTRTHitsR", ";N TRT hits;Pairs", 100, 0, 100);
  m_hnSCTHitsT = new TH1F(d+"hnSCTHitsT", ";N SCT hits;Pairs", 100, 0, 100);
  m_hnSCTHitsNoT = new TH1F(d+"hnSCTHitsNoT", ";N SCT hits;Pairs", 100, 0, 100);
  m_hnSCTHitsNoTR = new TH1F(d+"hnSCTHitsNoTR", ";N SCT hits;Pairs", 100, 0, 100);
  m_hnSCTHitsTR = new TH1F(d+"hnSCTHitsTR", ";N SCT hits;Pairs", 100, 0, 100);
  m_hnSCTHitsR = new TH1F(d+"hnSCTHitsR", ";N SCT hits;Pairs", 100, 0, 100);
  m_hnPixelHitsT = new TH1F(d+"hnPixelHitsT", ";N Pixel hits;Pairs", 100, 0, 100);
  m_hnPixelHitsNoT = new TH1F(d+"hnPixelHitsNoT", ";N Pixel hits;Pairs", 100, 0, 100);
  m_hnPixelHitsNoTR = new TH1F(d+"hnPixelHitsNoTR", ";N Pixel hits;Pairs", 100, 0, 100);
  m_hnPixelHitsTR = new TH1F(d+"hnPixelHitsTR", ";N Pixel hits;Pairs", 100, 0, 100);
  m_hnPixelHitsR = new TH1F(d+"hnPixelHitsR", ";N Pixel hits;Pairs", 100, 0, 100);

  m_hmuActualT = new TH1F(d+"hmuActualT", "actual #mu;actual #mu;Pairs", 40, 0, 40);
  m_hmuActualNoT = new TH1F(d+"hmuActualNoT", "actual #mu;actual #mu;Pairs", 40, 0, 40);
  m_hmuActualNoTR = new TH1F(d+"hmuActualNoTR", "actual #mu;actual #mu;Pairs", 40, 0, 40);
  m_hmuActualTR = new TH1F(d+"hmuActualTR", "actual #mu;actual #mu;Pairs", 40, 0, 40);
  m_hmuActualR = new TH1F(d+"hmuActualR", "actual #mu;actual #mu;Pairs", 40, 0, 40);

  m_hVertexMatchWeightT = new TH1F(d+"hVertexMatchWeightT", "vertexMatchWeight;vertexMatchWeight;Pairs", 50, 0, 1.0);
  m_hVertexMatchWeightNoT = new TH1F(d+"hVertexMatchWeightNoT", "vertexMatchWeight;vertexMatchWeight;Pairs", 50, 0, 1.0);
  m_hVertexMatchWeightNoTR = new TH1F(d+"hVertexMatchWeightNoTR", "vertexMatchWeight;vertexMatchWeight;Pairs", 50, 0, 1.0);
  m_hVertexMatchWeightTR = new TH1F(d+"hVertexMatchWeightTR", "vertexMatchWeight;vertexMatchWeight;Pairs", 50, 0, 1.0);
  m_hVertexMatchWeightR = new TH1F(d+"hVertexMatchWeightR", "vertexMatchWeight;vertexMatchWeight;Pairs", 50, 0, 1.0);

  m_hTruthMatchProbabilityT = new TH1F(d+"hTruthMatchProbabilityT", "truthMatchProbability;truthMatchProbability;Pairs", 50, 0, 1.0);
  m_hTruthMatchProbabilityNoT = new TH1F(d+"hTruthMatchProbabilityNoT", "truthMatchProbability;truthMatchProbability;Pairs", 50, 0, 1.0);
  m_hTruthMatchProbabilityNoTR = new TH1F(d+"hTruthMatchProbabilityNoTR", "truthMatchProbability;truthMatchProbability;Pairs", 50, 0, 1.0);
  m_hTruthMatchProbabilityTR = new TH1F(d+"hTruthMatchProbabilityTR", "truthMatchProbability;truthMatchProbability;Pairs", 50, 0, 1.0);
  m_hTruthMatchProbabilityR = new TH1F(d+"hTruthMatchProbabilityR", "truthMatchProbability;truthMatchProbability;Pairs", 50, 0, 1.0);

  m_hNTracks = new TH1F(d+"hNTracks", "N Tracks;Number of Tracks;Events", 130, 0-0.5, 1300-0.5);
  m_hNTruthMatched = new TH1F(d+"hNTruthMatched", "N TruthMatched;Number of Tracks;Events", 100, -0.5, 200-0.5);
  m_hNPassTrackSel = new TH1F(d+"hNPassTrackSel", "N PassTrackSel;Number of Tracks;Events", 100, -0.5, 200-0.5);
  m_hNCorrectParticle = new TH1F(d+"hNCorrectParticle", "N CorrectParticle;Number of Tracks;Events", 100, -0.5, 200-0.5);
  m_hNTrackVertexPairs = new TH1F(d+"hNTrackVertexPairs", "N TrackVertexPairs;Number of Pairs;Events", 100, -0.5, 600-0.5);
  m_hNTruthAssoc = new TH1F(d+"hNTruthAssoc", "N TruthAssoc;Number of Pairs;Events", 500, -0.5, 500-0.5);
  m_hNRecoAssoc = new TH1F(d+"hNRecoAssoc", "N RecoAssoc;Number of Pairs;Events", 65, -0.5, 260-0.5);
  m_hNMissed = new TH1F(d+"hNMissed", "N Missed;Number of Pairs;Events", 75, -0.5, 300-0.5);
  m_hNFake = new TH1F(d+"hNFake", "N Fake;Number of Pairs;Events", 45, -0.5, 45-0.5);
  m_hNCorrect = new TH1F(d+"hNCorrect", "N Correct;Number of Pairs;Events", 110, -0.5, 220-0.5);
  m_hNVertices = new TH1F(d+"hNVertices", "N Vertices;Number of Vertices;Events", 40, -0.5, 39.5);

  m_hCutflowTracks = new TH1F(d+"hCutflowTracks", "Cutflow (Tracks);;Tracks", 5, -0.5, 4.5);
  TAxis *xa = m_hCutflowTracks->GetXaxis();
  xa->SetBinLabel(1, "Reco Tracks");
  xa->SetBinLabel(2, "Pass Track Selection");
  xa->SetBinLabel(3, "Matched to Truth");
  xa->SetBinLabel(4, "Pass Particle Cut");
  xa->SetBinLabel(5, "Has Truth Vertex");

  m_hCutflowVertices = new TH1F(
      d+"hCutflowVertices", "Cutflow (Vertices);;Vertices",
      cvCOUNT, -0.5, cvCOUNT-0.5);
  xa = m_hCutflowVertices->GetXaxis();
  xa->SetBinLabel(cvAll+1, "Reco Vertices");
  xa->SetBinLabel(cvMatched+1, "Matched to Truth");
  xa->SetBinLabel(cvCorrectType+1, "Correct MatchType");

  m_hCutflowPairs = new TH1F(d+"hCutflowPairs", "Cutflow (Pairs);;Pairs", CutflowPairs::COUNT, -0.5, CutflowPairs::COUNT-0.5);
  xa = m_hCutflowPairs->GetXaxis();
  xa->SetBinLabel(CutflowPairs::All+1, "All Pairs");
  xa->SetBinLabel(CutflowPairs::Truth+1, "Truth Association (T)");
  xa->SetBinLabel(CutflowPairs::Correct+1, "Correct (T+R)");
  xa->SetBinLabel(CutflowPairs::Reco+1, "Reco Association (R)");
  xa->SetBinLabel(CutflowPairs::Fake+1, "Fake (!T+R)");
  xa->SetBinLabel(CutflowPairs::Missed+1, "Missed (T+!R)");
  xa->SetBinLabel(CutflowPairs::NoTruth+1, "No Truth (!T)");

  std::vector<Float_t> distBins;
  float xmin = 0.001;
  float xmax = 100;
  float lxmin = log10(xmin);
  float lxmax = log10(xmax);
  float lxstep = (lxmax-lxmin)/100;
  for (float lx = lxmin; lx <= lxmax; lx += lxstep) {
    float x = pow(10, lx);
    distBins.push_back(x);
    // Info("histInitialize", "x: %g", x);
  }
  UInt_t ndistBins = distBins.size()-1;
  m_hVertexMatchDistance = new TH1F(d+"hVertexMatchDistance", ";d(truth Vx, reco Vx) [mm];N matched vertices", ndistBins, &distBins[0]);

  using InDetVertexTruthMatchUtils::VertexMatchType;
  m_hVertexMatchTypeAll = new TH1F(
      d+"hVertexMatchTypeAll",
      "Vertex match type (all vertices);Vertex match type (all vertices);Vertices",
      VertexMatchType::NTYPES, -0.5, VertexMatchType::NTYPES-0.5);
  xa = m_hVertexMatchTypeAll->GetXaxis();
  for (int i = 0; i < VertexMatchType::NTYPES; i++)
    xa->SetBinLabel(i+1, toString((VertexMatchType) i));
  m_hVertexMatchDistance->SetDrawOption("text00 hist");

  m_hVertexMatchType = new TH1F(d+"hVertexMatchType",
      "Vertex match type (used vertices);Vertex match type (used vertices);Vertices",
      VertexMatchType::NTYPES, -0.5, VertexMatchType::NTYPES-0.5);
  xa = m_hVertexMatchType->GetXaxis();
  for (int i = 0; i < VertexMatchType::NTYPES; i++)
    xa->SetBinLabel(i+1, toString((VertexMatchType) i));
  m_hVertexMatchDistance->SetDrawOption("text00 hist");

  m_hTruthVertexBarcode = new TH1F(d+"hTruthVertexBarcode", ";Barcode (truth vertex);N matched vertices", 100, -1e6, 1e6);
  // m_hVertexMatchDistance = new TH1F(d+"hVertexMatchDistance", ";d(truth Vx, reco Vx) [mm];Events", 100, 0, 50);

  // m_hIPtT = new TH1F("hIPtT", "1/track pt (truth assoc)", 100, 1./20000,
  // 1./1);
  // m_hIPtTR = new TH1F("hIPtTR", "1/track pt (truth&reco assoc)", 100,
  // 1./20000, 1./1);

  wk()->addOutput(m_hPtT);
  wk()->addOutput(m_hPtNoT);
  wk()->addOutput(m_hPtNoTR);
  wk()->addOutput(m_hPtTR);
  wk()->addOutput(m_hPtR);

  wk()->addOutput(m_hPtVariableT);
  wk()->addOutput(m_hPtVariableNoT);
  wk()->addOutput(m_hPtVariableNoTR);
  wk()->addOutput(m_hPtVariableTR);
  wk()->addOutput(m_hPtVariableR);

  wk()->addOutput(m_hphiT);
  wk()->addOutput(m_hphiNoT);
  wk()->addOutput(m_hphiNoTR);
  wk()->addOutput(m_hphiTR);
  wk()->addOutput(m_hphiR);

  wk()->addOutput(m_hetaT);
  wk()->addOutput(m_hetaNoT);
  wk()->addOutput(m_hetaNoTR);
  wk()->addOutput(m_hetaTR);
  wk()->addOutput(m_hetaR);

  wk()->addOutput(m_heT);
  wk()->addOutput(m_heNoT);
  wk()->addOutput(m_heNoTR);
  wk()->addOutput(m_heTR);
  wk()->addOutput(m_heR);

  wk()->addOutput(m_hmT);
  wk()->addOutput(m_hmNoT);
  wk()->addOutput(m_hmNoTR);
  wk()->addOutput(m_hmTR);
  wk()->addOutput(m_hmR);

  wk()->addOutput(m_hd0T);
  wk()->addOutput(m_hd0NoT);
  wk()->addOutput(m_hd0NoTR);
  wk()->addOutput(m_hd0TR);
  wk()->addOutput(m_hd0R);
  wk()->addOutput(m_hd0sigT);
  wk()->addOutput(m_hd0sigNoT);
  wk()->addOutput(m_hd0sigNoTR);
  wk()->addOutput(m_hd0sigTR);
  wk()->addOutput(m_hd0sigR);

  wk()->addOutput(m_hdzSinThetaT);
  wk()->addOutput(m_hdzSinThetaNoT);
  wk()->addOutput(m_hdzSinThetaNoTR);
  wk()->addOutput(m_hdzSinThetaTR);
  wk()->addOutput(m_hdzSinThetaR);

  wk()->addOutput(m_hchi2T);
  wk()->addOutput(m_hchi2NoT);
  wk()->addOutput(m_hchi2NoTR);
  wk()->addOutput(m_hchi2TR);
  wk()->addOutput(m_hchi2R);
  wk()->addOutput(m_hchi2NDFT);
  wk()->addOutput(m_hchi2NDFNoT);
  wk()->addOutput(m_hchi2NDFNoTR);
  wk()->addOutput(m_hchi2NDFTR);
  wk()->addOutput(m_hchi2NDFR);
  wk()->addOutput(m_hnTRTHitsT);
  wk()->addOutput(m_hnTRTHitsNoT);
  wk()->addOutput(m_hnTRTHitsNoTR);
  wk()->addOutput(m_hnTRTHitsTR);
  wk()->addOutput(m_hnTRTHitsR);
  wk()->addOutput(m_hnSCTHitsT);
  wk()->addOutput(m_hnSCTHitsNoT);
  wk()->addOutput(m_hnSCTHitsNoTR);
  wk()->addOutput(m_hnSCTHitsTR);
  wk()->addOutput(m_hnSCTHitsR);
  wk()->addOutput(m_hnPixelHitsT);
  wk()->addOutput(m_hnPixelHitsNoT);
  wk()->addOutput(m_hnPixelHitsNoTR);
  wk()->addOutput(m_hnPixelHitsTR);
  wk()->addOutput(m_hnPixelHitsR);

  wk()->addOutput(m_hmuActualT);
  wk()->addOutput(m_hmuActualNoT);
  wk()->addOutput(m_hmuActualNoTR);
  wk()->addOutput(m_hmuActualTR);
  wk()->addOutput(m_hmuActualR);

  wk()->addOutput(m_hVertexMatchWeightT);
  wk()->addOutput(m_hVertexMatchWeightNoT);
  wk()->addOutput(m_hVertexMatchWeightNoTR);
  wk()->addOutput(m_hVertexMatchWeightTR);
  wk()->addOutput(m_hVertexMatchWeightR);

  wk()->addOutput(m_hTruthMatchProbabilityT);
  wk()->addOutput(m_hTruthMatchProbabilityNoT);
  wk()->addOutput(m_hTruthMatchProbabilityNoTR);
  wk()->addOutput(m_hTruthMatchProbabilityTR);
  wk()->addOutput(m_hTruthMatchProbabilityR);

  wk()->addOutput(m_hNTracks);
  wk()->addOutput(m_hNTruthMatched);
  wk()->addOutput(m_hNPassTrackSel);
  wk()->addOutput(m_hNCorrectParticle);
  wk()->addOutput(m_hNTrackVertexPairs);
  wk()->addOutput(m_hNTruthAssoc);
  wk()->addOutput(m_hNRecoAssoc);
  wk()->addOutput(m_hNMissed);
  wk()->addOutput(m_hNFake);
  wk()->addOutput(m_hNCorrect);
  wk()->addOutput(m_hNVertices);
  wk()->addOutput(m_hCutflowTracks);
  wk()->addOutput(m_hCutflowVertices);
  wk()->addOutput(m_hCutflowPairs);

  wk()->addOutput(m_hVertexMatchDistance);
  wk()->addOutput(m_hVertexMatchTypeAll);
  wk()->addOutput(m_hVertexMatchType);
  wk()->addOutput(m_hTruthVertexBarcode);

  // wk()->addOutput(m_hIPtT);
  // wk()->addOutput(m_hIPtTR);
  TMetaRecord *meta = new TMetaRecord(d+"MetaRecord", TString("MetaRecord"));
  meta->fields["description"] = description;
  meta->fields["sampleName"] = wk()->metaData()->getString("sample_name");
  meta->fields["inputFile"] = wk()->inputFile()->GetName();
  meta->fields["associationMethod"] = associationMethod;
  if (doTrackCut) {
    meta->fields["trackCutLevel"] = trackCutLevel;
    std::ostringstream os; os << trackMinPt;
    meta->fields["trackMinPt"] = os.str();
  }
  if (trackAbsPdgId >= 0) {
    std::ostringstream os;
    os << trackAbsPdgId
       << " (" << getParticleName(trackAbsPdgId) << ")";
    meta->fields["trackAbsPdgId"] = os.str();
  } else {
    meta->fields["trackAbsPdgId"] = "0 (no cut)";
  }
  meta->fields["doTrackCut"] = doTrackCut ? "true" : "false";
  wk()->addOutput(meta);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TVATester::fileExecute()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  // Info("fileExecute", "---");
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TVATester::changeInput(bool firstFile)
{
  (void)firstFile;
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  Info("changeInput", "filename: %s", wk()->inputFile()->GetName());
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TVATester::initialize()
{
  Info("initialize", "--");
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  std::string prefix{""};
  if (directoryName != "") {
    prefix = directoryName + "_";
  }

  CP::ITrackVertexAssociationTool *tool{0};
  if (associationMethod == "Electron") {
    tool = new CP::ElectronTrackVertexAssociationTool(prefix+"Electron");
  } else if (associationMethod == "Muon") {
    tool = new CP::MuonTrackVertexAssociationTool(prefix+"Muon");
  } else if (associationMethod == "Tight") {
    tool = new CP::TightTrackVertexAssociationTool(prefix+"Tight");
  } else if (associationMethod == "Loose") {
    tool = new CP::LooseTrackVertexAssociationTool(prefix+"Loose");
  } else {
    Error("initialize", "Unknown associationMethod '%s'", associationMethod.c_str());
    return EL::StatusCode::FAILURE;
  }

  // auto tool = new CP::MuonTrackVertexAssociationTool("Muon");
  // auto tool = new CP::BaseTrackVertexAssociationTool("Base");
  // CHECK_EXC(tool->setProperty("d0sig_cut", 5));
  // CHECK_EXC(tool->setProperty("dzSinTheta_cut", 0.5));
  // auto tool = new CP::TightTrackVertexAssociationTool("Tight");
  // auto tool = new CP::LooseTrackVertexAssociationTool(prefix+"Loose");
  // CHECK_EXC(tool->setProperty("d0sig_cut", 5));
  CHECK_EXC(tool->initialize());
  // todo: put in a vector etc so we can test multiple tools
  m_tvatool = tool;

  m_matchTool = new InDetVertexTruthMatchTool(prefix+"MatchTool");
  // m_matchTool->msg().setLevel(MSG::DEBUG);
  CHECK_EXC( m_matchTool->setProperty("trackMatchProb", 0.7) );
  CHECK_EXC( m_matchTool->setProperty("vertexMatchWeight", 0.7) );
  CHECK_EXC( m_matchTool->initialize() );

  // Todo: it would be nice if we could set this from the steering
  // script, but that only seems to be possible in athena
  if (doTrackCut) {
    m_trackSel = new InDet::InDetTrackSelectionTool(prefix+"TrackSelection");
    CHECK_EXC( m_trackSel->setProperty("CutLevel", trackCutLevel) );
    CHECK_EXC( m_trackSel->setProperty("minPt", trackMinPt) );
    CHECK_EXC( m_trackSel->initialize() );
  }

  m_event = wk()->xaodEvent();

  return EL::StatusCode::SUCCESS;
}

/// Like Info, but indents the message a bit, determined by the `indent`
/// parameter.
/// Useful if you want to output e.g. decay hierarchies.
template <typename... T>
void TVATester::myInfo(const char *location, uint indent, const char *msgfmt,
                       T... args) const
{
  std::string ws(indent * 2, ' ');
  ws += msgfmt;
  Info(location, ws.c_str(), args...);
}

/// Dumps information about a xAOD::TruthVertex.  You can also specify a reco
/// vertex in `pv` and it will calculate the distance.
void TVATester::dump(const char *location, const xAOD::TruthVertex *vx,
                     const xAOD::Vertex *pv, uint nparents, uint indent) const
{
  if (!vx) {
    myInfo(location, indent, "--(null)");
    return;
  }
  // myInfo(location, indent, "--vx: nparents: %d indent: %d", nparents,
  // indent);
  myInfo(location, indent, "--vx: %10.3f %10.3f %10.3f %10.3f", vx->x(),
         vx->y(), vx->z(), vx->t());
  if (pv) {
    double distTvPv =
        sqrt(pow(vx->x() - pv->x(), 2) + pow(vx->y() - pv->y(), 2) +
             pow(vx->z() - pv->z(), 2));
    // pow(vx->z() - pv->z(), 2) + pow(vx->t(), 2));
    myInfo(location, indent, "  dist: %f", distTvPv);
  }
  myInfo(location, indent, "  barcode: %d", vx->barcode());
  myInfo(location, indent, "  nOutgoing: %zu, nIncoming: %zu",
         vx->nOutgoingParticles(), vx->nIncomingParticles());

  if (nparents > 0) {
    if (vx->nIncomingParticles() != 1)
      return;

    for (uint i = 0; i < vx->nIncomingParticles(); i++) {
      auto incoming = vx->incomingParticle(i);
      myInfo(location, indent, "  incoming: %s, status: %d",
             getParticleName(incoming->pdgId()), incoming->status());
      dump(location, incoming->prodVtx(), pv, nparents - 1, indent + 1);
    }
  }
}

const xAOD::TruthVertex *
TVATester::getClosestTruthVertex(const xAOD::Vertex *rv) const
{
  double minDistance = std::numeric_limits<double>::max();
  const xAOD::TruthVertex *tvClosest{0};

  const xAOD::TruthVertexContainer *tvc{0};
  CHECK_EXC(m_event->retrieve(tvc, "TruthVertices"));

  for (const auto tv : *tvc) {
    // if (abs(tv->barcode()) >= 200000 || tv->t() > 0) continue;
    // if (abs(tv->barcode()) >= 200000) continue;
    if (rv->vertexType() == xAOD::VxType::PriVtx) {
      if (abs(tv->barcode()) >= 200000)
        continue;
    } else {
      if (abs(tv->barcode()) < 200000)
        continue;
    }

    if (tv->t() > 0)
      continue;

    // Info("execute", "tv->barcode() %d", tv->barcode());
    double distTvPv =
        sqrt(pow(tv->x() - rv->x(), 2) + pow(tv->y() - rv->y(), 2) +
             pow(tv->z() - rv->z(), 2));
    // pow(tv->z() - rv->z(), 2) + pow(tv->t(), 2));
    // double distTvPv = sqrt( pow(tv->x() - rv->x(), 2) + pow(tv->y() -
    // rv->y(), 2) + pow(tv->z() - rv->z(), 2) );
    // Info("execute", "t %f", tv->t());
    // Info("execute", "d %f", distTvPv);
    if (distTvPv < minDistance) {
      minDistance = distTvPv;
      tvClosest = tv;
    }
  }
  return tvClosest;
}

static const char* toString(xAOD::VxType::VertexType type) {
  using xAOD::VxType::VertexType;
  switch (type) {
    case VertexType::NoVtx:        return "NoVtx";
    case VertexType::PriVtx:       return "PriVtx";
    case VertexType::SecVtx:       return "SecVtx";
    case VertexType::PileUp:       return "PileUp";
    case VertexType::ConvVtx:      return "ConvVtx";
    case VertexType::V0Vtx:        return "V0Vtx";
    case VertexType::KinkVtx:      return "KinkVtx";
    case VertexType::NotSpecified: return "NotSpecified";
  }
  return "INVALID";
}

static const char* toString(InDetVertexTruthMatchUtils::VertexMatchType type) {
  using InDetVertexTruthMatchUtils::VertexMatchType;
  switch (type) {
    case VertexMatchType::MATCHED: return "MATCHED";
    case VertexMatchType::MERGED:  return "MERGED";
    case VertexMatchType::SPLIT:   return "SPLIT";
    case VertexMatchType::FAKE:    return "FAKE";
    case VertexMatchType::DUMMY:   return "DUMMY";
    case VertexMatchType::NTYPES:;
  }
  return "INVALID";
}

EL::StatusCode TVATester::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  const bool debug = false;

  const xAOD::EventInfo *event{0};
  CHECK_EXC(m_event->retrieve(event, "EventInfo"));
  if (debug) {
    Info("execute", "-----------------------------------------");
    Info("execute", "Run %d, event number %llu", event->runNumber(), event->eventNumber());
  }

  const xAOD::TrackParticleContainer *tracks{0};
  CHECK_EXC(m_event->retrieve(tracks, "InDetTrackParticles"));

  const xAOD::VertexContainer *vertices;
  CHECK_EXC(m_event->retrieve(vertices, "PrimaryVertices"));

  const xAOD::TruthEventContainer *truthEvents{0};
  CHECK_EXC( m_event->retrieve(truthEvents, "TruthEvents") );
  if (debug) {
    Info("execute", "%lu truth events", truthEvents->size());
  }

  // const xAOD::TruthPileupEventContainer *truthPileupEvents{0};
  // CHECK_EXC( m_event->retrieve(truthPileupEvents, "TruthPileupEvents") );
  // Info("execute", "%lu truth pileup events", truthPileupEvents->size());

  CHECK_EXC( m_matchTool->matchVertices(*vertices) );

  int nCorrectParticle = 0;
  int nPassTrackSel = 0;
  int nTrackVertexPairs = 0;
  int nTruthMatched = 0;
  int nTruthAssoc = 0;
  int nRecoAssoc = 0;
  int nMissed = 0;
  int nFake = 0;
  int nCorrect = 0;
  int nVertices = 0;
  std::vector<const xAOD::TrackParticle *> goodTracks;

  for (const auto trk : *tracks) {
    m_hCutflowTracks->Fill(0);

    if (m_trackSel && !m_trackSel->accept(trk))
      continue;
    nPassTrackSel++;
    m_hCutflowTracks->Fill(1);

    const xAOD::TruthParticle *truth =
        xAOD::TruthHelpers::getTruthParticle(*trk);
    if (!truth)
      continue;
    nTruthMatched++;
    m_hCutflowTracks->Fill(2);

    if (trackAbsPdgId > 0 && truth->absPdgId() != trackAbsPdgId) {
      continue;
    }
    m_hCutflowTracks->Fill(3);
    nCorrectParticle++;

    auto tvFromTruthTrack = truth->prodVtx();
    if (tvFromTruthTrack == nullptr) {
      // no production vertex in truth, skip this track
      continue;
    }
    m_hCutflowTracks->Fill(4);
    // todo: nTrackHasTruthVertex++;

    goodTracks.push_back(trk);
  } // loop over all tracks

  // accessor for the vertex matching type
  xAOD::Vertex::Decorator<InDetVertexTruthMatchUtils::VertexMatchType> getMatchType("VertexMatchType");
  xAOD::Vertex::Decorator<std::vector<InDetVertexTruthMatchUtils::VertexTruthMatchInfo>> getMatchInfo("TruthEventMatchingInfos");

  // accessor for the track's truth match probability
  xAOD::TrackParticle::ConstAccessor<float> trk_truthProbAcc("truthMatchProbability");

  // outer loop over all vertices
  // we could also iterate over InDetVertexTruthMatchUtils::hardScatterMatches?
  for (auto recoVertex : *vertices) {
    m_hCutflowVertices->Fill(cvAll);
    auto matchType = getMatchType(*recoVertex);
    if (debug) {
      Info("execute", "Vertex type: %s %d", toString(recoVertex->vertexType()), recoVertex->vertexType());
      Info("execute", "Vertex match type: %s %d", toString(matchType), matchType);
    }
    m_hVertexMatchTypeAll->Fill(matchType);

    // const xAOD::TruthVertex *tvClosest = nullptr;
    const xAOD::TruthVertex *tvMatched = nullptr;
    double vertexMatchWeight = 0.;

    if (matchMethod == "smart") {
      auto infos = getMatchInfo(*recoVertex);
      for (const auto infopair: infos) {
        auto link = infopair.first;
        float weight = infopair.second;
        // Info("execute", "  valid: %d", link.isValid());a
        if (!link.isValid()) {
          if (debug) {
            Info("execute", "  Fakes, weight: %f", weight);
          }
        } else if ((*link)->type() == xAOD::Type::TruthEvent) {
          const xAOD::TruthEvent* truthEvent = dynamic_cast<const xAOD::TruthEvent *>(*link);
          auto tvFirst = truthEvent->truthVertex(0);
          tvMatched = tvFirst;
          vertexMatchWeight = weight;
          break;
          if (debug) {
            auto tvSignal = truthEvent->signalProcessVertex();
            Info("execute", "  Truth event: %p, weight: %f", static_cast<const void *>(truthEvent), weight);
            Info("execute", "    %zu truth vertices", truthEvent->nTruthVertices());
            Info("execute", "    signalProcessVertex:");
            dump("execute", tvSignal, recoVertex, 6, 2);
            Info("execute", "    truthVertex(0):");
            dump("execute", tvFirst, recoVertex, 6, 2);
          }
        } else if ((*link)->type() == xAOD::Type::TruthPileupEvent) {
          const xAOD::TruthPileupEvent* truthPileupEvent = dynamic_cast<const xAOD::TruthPileupEvent *>(*link);
          auto tvFirst = truthPileupEvent->truthVertex(0);
          tvMatched = tvFirst;
          vertexMatchWeight = weight;
          break;
          if (debug) {
            Info("execute", "  Truth event: %p, weight: %f", static_cast<const void *>(truthPileupEvent), weight);
            Info("execute", "    %zu truth vertices", truthPileupEvent->nTruthVertices());
            Info("execute", "    truthVertex(0):");
            dump("execute", tvFirst, recoVertex, 6, 2);
          }
        }
      }
    } else if (matchMethod == "nearest") {
      tvMatched = getClosestTruthVertex(recoVertex);
    } else {
      Error("execute", "Invalid matchMethod '%s' specified.", matchMethod.c_str());
    }

    // // try to match reco to a truth vertex
    // tvClosest = getClosestTruthVertex(recoVertex);
    // Info("execute", "closest truth vertex:");
    // dump("execute", tvClosest, recoVertex, 6);

    // if (!tvClosest) {
    if (!tvMatched) {
      if (debug) {
        Warning("execute", "could not find a truth vertex");
      }
      continue;
    }
    m_hCutflowVertices->Fill(cvMatched);

    // for the smart matching method, check if the matching type is OK
    if (matchMethod == "smart") {
      using InDetVertexTruthMatchUtils::VertexMatchType;
      if (matchType == VertexMatchType::FAKE || matchType == VertexMatchType::DUMMY) {
        continue;
      }
    }
    m_hCutflowVertices->Fill(cvCorrectType);

    nVertices++;

    double distTvPv =
        sqrt(pow(tvMatched->x() - recoVertex->x(), 2) + pow(tvMatched->y() - recoVertex->y(), 2) +
             pow(tvMatched->z() - recoVertex->z(), 2));
    m_hVertexMatchDistance->Fill(distTvPv);
    m_hTruthVertexBarcode->Fill(tvMatched->barcode());
    m_hVertexMatchType->Fill(matchType);

    // inner loop over all tracks
    for (const auto trk : goodTracks) {
      // Info("execute", " ");
      // Info("execute", "tvClosest    x y z: %10.3f %10.3f %10.3f %10.3f",
      //      tvClosest->x(), tvClosest->y(), tvClosest->z(), tvClosest->t());
      // Info("execute", "reco rv      x y z: %10.3f %10.3f %10.3f",
      //      rv->x(), rv->y(), rv->z());
      //
      m_hCutflowPairs->Fill(CutflowPairs::All);
      nTrackVertexPairs++;

      // This should give THE matched vertex for a given track
      auto vxLink = m_tvatool->getUniqueMatchVertexLink(*trk, *vertices);
      bool recoAssoc = vxLink.isValid() && ((*vxLink) == recoVertex);

      // // This in contrast should say if a trk and recoVertex are
      // // compatible, not if recoVertex is the best match.
      // bool recoAssoc = m_tvatool->isCompatible(*trk, *recoVertex);

      bool truthAssoc = false;
      const xAOD::TruthParticle *truth =
          xAOD::TruthHelpers::getTruthParticle(*trk);

      // assert(truth != nullptr);

      auto tvFromTruthTrack = truth->prodVtx();
      if (!tvFromTruthTrack) {
        Warning("execute", "truth track has no production vertex!");
        Warning("execute", "pdgId: %d",
                truth->pdgId());
        Warning("execute", "track: %s",
                getParticleName(truth->pdgId()));
        Warning("execute", "pt: %f",
                truth->pt());
        // Warning("execute", "track: %s, pt: %f",
                // pdg.GetParticle(truth->pdgId())->GetName(), truth->pt());
        dump("execute", tvFromTruthTrack, recoVertex, 6);
        continue;
      }

      // Info("execute", "tvFromTruthTrack x y z: %10.3f %10.3f %10.3f %10.3f",
      //      tvFromTruthTrack->x(), tvFromTruthTrack->y(),
      //      tvFromTruthTrack->z(),
      //      tvFromTruthTrack->t());
      // Info("execute", "status: %d", truth->status());

      // if (recoAssoc || truthAssoc) {
      //     Info("execute", "Truth vertex from truth track:");
      //     dump("execute", tvFromTruthTrack, rv, 6);
      //     Info("execute", "compare with tvClosest:");
      //     dump("execute", tvClosest, rv, 6);
      // }

      // // Attempt to find the first ancestor vertex.  In this case we are
      // // not actually interested in vertices, but in (sub-)events.
      // auto getAncestor = [](const xAOD::TruthVertex *tv) {
      //   while (tv->nIncomingParticles() == 1) {
      //     auto ttTmp = tv->incomingParticle(0);
      //     if (!ttTmp) break;
      //     auto tvTmp = ttTmp->prodVtx();
      //     if (!tvTmp) break;
      //     tv = tvTmp;
      //   }
      //   return tv;
      // };

      // const xAOD::TruthVertex *tvMatchedAncestor =
      //   getAncestor(tvMatched);
      // const xAOD::TruthVertex *tvFromTruthTrackAncestor =
      //   getAncestor(tvFromTruthTrack);

      // if (tvFromTruthTrackAncestor->x() == tvMatchedAncestor->x() &&
      //     tvFromTruthTrackAncestor->y() == tvMatchedAncestor->y() &&
      //     tvFromTruthTrackAncestor->z() == tvMatchedAncestor->z()) {
      //   truthAssoc = true;
      // }

      // Check if the truth vertex from the truth track
      // equals the truth vertex from the reco vertex.
      // If that is true, we have a truth association.
      if (tvFromTruthTrack->x() == tvMatched->x() &&
          tvFromTruthTrack->y() == tvMatched->y() &&
          tvFromTruthTrack->z() == tvMatched->z()) {
        truthAssoc = true;
      }

      float truthMatchProbability = trk_truthProbAcc(*trk);
      float vx_z0 = recoVertex->z();
      float trk_z0 = trk->z0();
      float beamspot_z0 = trk->vz();
      float theta = trk->theta();
      double d0sig = xAOD::TrackingHelpers::d0significance(
          trk, event->beamPosSigmaX(), event->beamPosSigmaY(),
          event->beamPosSigmaXY());

      double dzSinTheta = (trk_z0 - vx_z0 + beamspot_z0) * sin(theta);
      uint8_t nTRTHits{0};
      uint8_t nSCTHits{0};
      uint8_t nPixelHits{0};
      if (!trk->summaryValue(nTRTHits, xAOD::numberOfTRTHits)) {
        Info("execute", "Warning, could not get number of TRT hits");
      }
      if (!trk->summaryValue(nSCTHits, xAOD::numberOfSCTHits)) {
        Info("execute", "Warning, could not get number of SCT hits");
      }
      if (!trk->summaryValue(nPixelHits, xAOD::numberOfPixelHits)) {
        Info("execute", "Warning, could not get number of Pixel hits");
      }
      float muActual = event->actualInteractionsPerCrossing();

      if (recoAssoc) {
        nRecoAssoc++;
        m_hCutflowPairs->Fill(CutflowPairs::Reco);
        m_hPtR->Fill(trk->pt() / 1000.);
        m_hPtVariableR->Fill(trk->pt() / 1000.);
        m_hphiR->Fill(trk->phi());
        m_hetaR->Fill(trk->eta());
	m_heR->Fill(trk->e() / 1000.);
        m_hmR->Fill(trk->m());
        m_hd0R->Fill(trk->d0());
        m_hd0sigR->Fill(d0sig);
        m_hdzSinThetaR->Fill(dzSinTheta);
        m_hchi2R->Fill(trk->chiSquared());
        m_hchi2NDFR->Fill(trk->chiSquared() / trk->numberDoF());
        m_hnTRTHitsR->Fill(nTRTHits);
        m_hnSCTHitsR->Fill(nSCTHits);
        m_hnPixelHitsR->Fill(nPixelHits);
        m_hmuActualR->Fill(muActual);
        m_hVertexMatchWeightR->Fill(vertexMatchWeight);
        m_hTruthMatchProbabilityR->Fill(truthMatchProbability);
      }
      if (truthAssoc) {
        nTruthAssoc++;
        m_hCutflowPairs->Fill(CutflowPairs::Truth);
        m_hPtT->Fill(trk->pt() / 1000.);
        m_hPtVariableT->Fill(trk->pt() / 1000.);
        m_hphiT->Fill(trk->phi());
        m_hetaT->Fill(trk->eta());
        m_heT->Fill(trk->e() / 1000.);
        m_hmT->Fill(trk->m());
        m_hd0T->Fill(trk->d0());
        m_hd0sigT->Fill(d0sig);
        m_hdzSinThetaT->Fill(dzSinTheta);
        m_hchi2T->Fill(trk->chiSquared());
        m_hchi2NDFT->Fill(trk->chiSquared() / trk->numberDoF());
        m_hnTRTHitsT->Fill(nTRTHits);
        m_hnSCTHitsT->Fill(nSCTHits);
        m_hnPixelHitsT->Fill(nPixelHits);
        m_hmuActualT->Fill(muActual);
        m_hVertexMatchWeightT->Fill(vertexMatchWeight);
        m_hTruthMatchProbabilityT->Fill(truthMatchProbability);
      } else {
        m_hCutflowPairs->Fill(CutflowPairs::NoTruth);
        m_hPtNoT->Fill(trk->pt() / 1000.);
        m_hPtVariableNoT->Fill(trk->pt() / 1000.);
        m_hphiNoT->Fill(trk->phi());
        m_hetaNoT->Fill(trk->eta());
        m_heNoT->Fill(trk->e() / 1000.);
        m_hmNoT->Fill(trk->m());
        m_hd0NoT->Fill(trk->d0());
        m_hd0sigNoT->Fill(d0sig);
        m_hdzSinThetaNoT->Fill(dzSinTheta);
        m_hchi2NoT->Fill(trk->chiSquared());
        m_hchi2NDFNoT->Fill(trk->chiSquared() / trk->numberDoF());
        m_hnTRTHitsNoT->Fill(nTRTHits);
        m_hnSCTHitsNoT->Fill(nSCTHits);
        m_hnPixelHitsNoT->Fill(nPixelHits);
        m_hmuActualNoT->Fill(muActual);
        m_hVertexMatchWeightNoT->Fill(vertexMatchWeight);
        m_hTruthMatchProbabilityNoT->Fill(truthMatchProbability);
      }
      if (truthAssoc && !recoAssoc) {
        nMissed++;
        m_hCutflowPairs->Fill(CutflowPairs::Missed);
      }
      if (!truthAssoc && recoAssoc) {
        nFake++;
        m_hCutflowPairs->Fill(CutflowPairs::Fake);
        m_hPtNoTR->Fill(trk->pt() / 1000.);
        m_hPtVariableNoTR->Fill(trk->pt() / 1000.);
        m_hphiNoTR->Fill(trk->phi());
        m_hetaNoTR->Fill(trk->eta());
        m_heNoTR->Fill(trk->e() / 1000.);
        m_hmNoTR->Fill(trk->m());
        m_hd0NoTR->Fill(trk->d0());
        m_hd0sigNoTR->Fill(d0sig);
        m_hdzSinThetaNoTR->Fill(dzSinTheta);
        m_hchi2NoTR->Fill(trk->chiSquared());
        m_hchi2NDFNoTR->Fill(trk->chiSquared() / trk->numberDoF());
        m_hnTRTHitsNoTR->Fill(nTRTHits);
        m_hnSCTHitsNoTR->Fill(nSCTHits);
        m_hnPixelHitsNoTR->Fill(nPixelHits);
        m_hmuActualNoTR->Fill(muActual);
        m_hVertexMatchWeightNoTR->Fill(vertexMatchWeight);
        m_hTruthMatchProbabilityNoTR->Fill(truthMatchProbability);
      }
      if (truthAssoc && recoAssoc) {
        nCorrect++;
        m_hCutflowPairs->Fill(CutflowPairs::Correct);
        m_hPtTR->Fill(trk->pt() / 1000.);
        m_hPtVariableTR->Fill(trk->pt() / 1000.);
        m_hphiTR->Fill(trk->phi());
        m_hetaTR->Fill(trk->eta());
        m_heTR->Fill(trk->e() / 1000.);
        m_hmTR->Fill(trk->m());
        m_hd0TR->Fill(trk->d0());
        m_hd0sigTR->Fill(d0sig);
        m_hdzSinThetaTR->Fill(dzSinTheta);
        m_hchi2TR->Fill(trk->chiSquared());
        m_hchi2NDFTR->Fill(trk->chiSquared() / trk->numberDoF());
        m_hnTRTHitsTR->Fill(nTRTHits);
        m_hnSCTHitsTR->Fill(nSCTHits);
        m_hnPixelHitsTR->Fill(nPixelHits);
        m_hmuActualTR->Fill(muActual);
        m_hVertexMatchWeightTR->Fill(vertexMatchWeight);
        m_hTruthMatchProbabilityTR->Fill(truthMatchProbability);
      }

    } // loop over all tracks

  } // loop over all vertices

  m_hNTracks->Fill(tracks->size());
  m_hNTruthMatched->Fill(nTruthMatched);
  m_hNPassTrackSel->Fill(nPassTrackSel);
  m_hNCorrectParticle->Fill(nCorrectParticle);
  m_hNTrackVertexPairs->Fill(nTrackVertexPairs);
  m_hNTruthAssoc->Fill(nTruthAssoc);
  m_hNRecoAssoc->Fill(nRecoAssoc);
  m_hNMissed->Fill(nMissed);
  m_hNFake->Fill(nFake);
  m_hNCorrect->Fill(nCorrect);
  m_hNVertices->Fill(nVertices);

  // Info("execute", "%d tracks, %d matched to truth, %d pass particle cut, %d
  // vertices",
  //      nTracks, nTruthMatched, nCorrectParticle, nVertices);
  // int nTotal = nVertices * nCorrectParticle;
  // Info("execute", "%d×%d = %d combinations, among those", nCorrectParticle,
  // nVertices, nTotal);
  // Info("execute", "  recoAssoc:  %4d       \t(reco trk compatible w/
  // vertex)", nRecoAssoc);
  // Info("execute", "  truthAssoc: %4d       \t(matched truth trk->vertex ==
  // matched truth vertex)",
  //      nTruthAssoc);
  // Info("execute", "  fake: %d/%d = %5.2f%% \t(reco matched but not truth)",
  // nFake, nRecoAssoc, 100. * nFake / nRecoAssoc);
  // Info("execute", "  missed: %d/%d = %5.2f%% \t(truth matched but not reco)",
  // nMissed, nTruthAssoc,
  //      100. * nMissed / nTruthAssoc);

  // Info("execute", "  correct: %d/%d = %5.2f%% \t(reco matched when truth
  // matched)", nCorrect, nTruthAssoc,
  //      100. * nCorrect / nTruthAssoc);

  // Info("execute", "%d tracks (%d compatible with PV), %d matched to truth (%d
  // compatible with PV)",
  // nTracks, nVxMatched, nTruthMatched, nTruthVxMatched);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TVATester::postExecute()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TVATester::finalize()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TVATester::histFinalize()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}

// vim: expandtab tabstop=8 shiftwidth=2 softtabstop=2
