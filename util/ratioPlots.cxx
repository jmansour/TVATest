
// ROOT includes
#include "TCanvas.h"
#include "TColor.h"
#include "TFile.h"
#include "TGraphAsymmErrors.h"
#include "TH1F.h"
#include "TIterator.h"
#include "TLegend.h"
#include "TLine.h"
#include "TMultiGraph.h"
#include "TROOT.h"
#include "TRegexp.h"
#include "TString.h"
#include "TStyle.h"
#include "TSystem.h"
#include "TError.h"
#include "TSystemDirectory.h"

// libpng
#define PNG_SKIP_SETJMP_CHECK
#include "png.h"

// local includes
#include "TVATest/TMetaRecord.h"
#include "AtlasStyle.h"

// c++ stdlib
#include <ctime>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <sstream>
#include <vector>
using namespace std;

void addPNGFields(const char *filename, map<string, string> fields);

enum class RatioType {fakerate, efficiency, fakerate_alt};
const float hdScale = 1.0;

std::string getTimeStr() {
  std::time_t t = std::time(nullptr);
  char timestr[100];
  if (std::strftime(timestr, sizeof(timestr), "%c %Z", std::localtime(&t))) {
    return timestr;
  }
  return "";
}

TGraphAsymmErrors *makeRatio(TString name, RatioType ratioType)
{
  TString numeratorName, denominatorName, outname, ytitle;
  switch (ratioType) {
  case RatioType::fakerate:
    numeratorName = "h" + name + "NoTR";
    denominatorName = "h" + name + "NoT";
    outname = "fake_" + name + ".png";
    //outname = "fake_" + name + ".C";
    ytitle = "Fake rate P(reco assoc | no truth assoc)";
    break;
  case RatioType::fakerate_alt:
    numeratorName = "h" + name + "NoTR";
    denominatorName = "h" + name + "R";
    outname = "fakealt_" + name + ".png";
    //outname = "fakealt_" + name + ".C";
    ytitle = "Fake rate P(no truth | reco assoc)";
    break;
  case RatioType::efficiency:
    numeratorName = "h" + name + "TR";
    denominatorName = "h" + name + "T";
    outname = "eff_" + name + ".png";
    //outname = "eff_" + name + ".C";
    ytitle = "Efficiency P(reco assoc | truth assoc)";
    break;
  }
  // Total number of pairs (100%, e.g. truth assoc only)
  TH1 *hNumerator = (TH1 *)gDirectory->Get(numeratorName);
  // Selected number of pairs (e.g. truth and reco assoc)
  TH1 *hDenominator = (TH1 *)gDirectory->Get(denominatorName);
  if (!hDenominator || !hNumerator) {
    Warning("makeRatio", "Histogram for %s not found in root file.",
            (const char *)name);
    return 0;
  }
  // TCanvas *can = new TCanvas();
  TCanvas *can = new TCanvas("can","can",200,10,800*hdScale,600*hdScale);
  TGraphAsymmErrors *graph = new TGraphAsymmErrors(hNumerator, hDenominator);
  graph->Draw("APEZ");
  graph->SetMarkerSize(0);
  graph->GetXaxis()->SetTitle(hDenominator->GetXaxis()->GetTitle());
  graph->GetYaxis()->SetTitle(ytitle);
  can->SaveAs(outname);

  auto meta = (TMetaRecord *)gDirectory->Get("MetaRecord");
  map<string, string> fields;
  fields["variable"] = name;
  fields["plotCreated"] = getTimeStr();

  if (meta) {
    for (auto pair : meta->fields) {
      fields[pair.first] = pair.second;
    }
  }
  addPNGFields(outname, fields);

  can->Close();

  return graph;
}

typedef void TObjectDeleter(TObject *);
void deleter(TObject *obj) { obj->Delete(); }
template <typename T> std::unique_ptr<T, TObjectDeleter *> unique(T *obj)
{
  return std::unique_ptr<T, TObjectDeleter *>(obj, &deleter);
}

/// Expands wildcards in path and returns one filename if found,
/// or an empty string otherwise.
TString findFile(TString path)
{
  TString dirname = gSystem->DirName(path);
  TString basename = gSystem->BaseName(path);
  TSystemDirectory dir(dirname, dirname);

  auto files = unique(dir.GetListOfFiles());
  if (files) {
    TRegexp pattern(basename, true);
    for (auto it : *files) {
      auto f = dynamic_cast<TSystemFile *>(it);
      TString fname = f->GetName();
      if (!f->IsDirectory() && fname.Contains(pattern)) {
        return gSystem->PrependPathName(dirname, fname);
      }
    }
  }
  return "";
}

/// List of variables to make efficiency ratios from
/// TODO: do not hardcode
static vector<string> ratioVars = {
  "Pt",
  "PtVariable",
  "phi",
  "eta",
  "m", //test by Bin
  "e", //test by Bin
  "d0",
  "d0sig",
  "dzSinTheta",
  "chi2",
  "chi2NDF",
  "nTRTHits",
  "nSCTHits",
  "nPixelHits",
  "muActual",
  "VertexMatchWeight",
  "TruthMatchProbability",
};

/// Makes a histogram for a variable, and saves the image
TH1 *makeHisto(TString name)
{
  TH1 *histo = (TH1 *)gDirectory->Get("h" + name);
  if (!histo)
    throw std::runtime_error("Error, histogram not found");
  // TCanvas *can = new TCanvas();
  TCanvas *can = new TCanvas("can","can",200,10,800*hdScale,600*hdScale);
  gStyle->SetOptStat(1111111);
  gStyle->SetStatFont(42);
  gStyle->SetStatBorderSize(1);
  if (name == "VertexMatchDistance") {
    can->SetLogx(true);
    can->SetLogy(true);
  }
  histo->Draw();
  TString outname = name + ".png";
  //TString outname = name + ".C";
  can->SaveAs(outname);
  can->Close();

  auto meta = (TMetaRecord *)gDirectory->Get("MetaRecord");
  map<string, string> fields;
  fields["variable"] = name;
  fields["plotCreated"] = getTimeStr();
  fields["stats.entries"] = TString::Format("%g", histo->GetEntries());
  fields["stats.underflow"] = TString::Format("%g", histo->GetBinContent(0));
  fields["stats.overflow"] =
      TString::Format("%g", histo->GetBinContent(histo->GetNbinsX() + 1));
  if (meta) {
    for (auto pair : meta->fields) {
      fields[pair.first] = pair.second;
    }
  }
  addPNGFields(outname, fields);

  return histo;
}

/// Makes all the plots for the current TFile-directory
map<string, TObject *> makePlots()
{
  map<string, TObject *> plots;

  for (string var : ratioVars) {
    auto *plot = makeRatio(var, RatioType::efficiency);
    if (plot) plots["eff_"+var] = plot;

    plot = makeRatio(var, RatioType::fakerate);
    if (plot) plots["fake_"+var] = plot;

    plot = makeRatio(var, RatioType::fakerate_alt);
    if (plot) plots["fakealt_"+var] = plot;
  }

  // vector<string> histogramNames;
  for (TObject *key : *gDirectory->GetListOfKeys()) {
    auto dirname = TString(key->GetName());
    TObject *obj = gDirectory->Get(dirname);
    if (!obj->InheritsFrom("TH1")) {
      continue;
    }

    auto varname = dirname(1, dirname.Length() - 1);
    // if (varname != "phiR") continue;
    cout << varname << endl;
    plots[varname.Data()] = makeHisto(varname);
  }

  return plots;
}

/// Writes field-values pairs into a png file.
void addPNGFields(const char *filename, map<string, string> fields)
{
  FILE *fp = fopen(filename, "rb");
  if (!fp)
    return;

  png_structp png_ptr =
      png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!png_ptr)
    return;

  png_infop info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr) {
    png_destroy_read_struct(&png_ptr, NULL, NULL);
    return;
  }

  png_infop end_info = png_create_info_struct(png_ptr);
  if (!end_info) {
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    return;
  }

  if (setjmp(png_jmpbuf(png_ptr))) {
    png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
    fclose(fp);
    return;
  }

  png_init_io(png_ptr, fp);
  png_set_keep_unknown_chunks(png_ptr, 1, NULL, 0);
  png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

  int num_text = 0;
  // png_get_text(png_ptr, info_ptr, &text_ptr, &num_text);

  fclose(fp);

  png_textp text_ptr = new png_text[fields.size()];

  num_text = 0;
  map<string, string>::iterator it;
  for (it = fields.begin(); it != fields.end(); it++) {
    // add one:
    num_text++;
    text_ptr[num_text - 1].key = (char *)it->first.c_str();
    text_ptr[num_text - 1].text = (char *)it->second.c_str();
    text_ptr[num_text - 1].compression = PNG_TEXT_COMPRESSION_NONE;
    text_ptr[num_text - 1].text_length = it->second.size();
  }
  png_set_text(png_ptr, info_ptr, text_ptr, num_text);

  FILE *fout = fopen(filename, "wb");
  if (!fout)
    return;

  // write
  png_structp pngw_ptr =
      png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  png_init_io(pngw_ptr, fout);
  png_write_png(pngw_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

  png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
  png_destroy_write_struct(&pngw_ptr, NULL);
  fclose(fout);
  delete[] text_ptr;
}

// A rectangle (in ROOT's pad coordinate system)
struct Rectangle {
  double x1, y1, x2, y2;
  Rectangle(double x1_, double y1_, double x2_, double y2_):
    x1(x1_), y1(y1_), x2(x2_), y2(y2_) {}

  static Rectangle from_lbwh(double left, double bottom, double width, double height) {
    return Rectangle(left, bottom, left+width, bottom+height);
  }

  double overlap(const Rectangle& other) const {
    double overlap_x = max(0., min(x2, other.x2) - max(x1, other.x1));
    double overlap_y = max(0., min(y2, other.y2) - max(y1, other.y1));
    return overlap_x * overlap_y;
  }

  Rectangle expanded(double dx, double dy) const {
    return Rectangle(x1-dx, y1-dy, x2+dx, y2+dy);
  }

  Rectangle histToNDC(const TPad *pad) const {
    double px1 = gPad->GetX1();
    double py1 = gPad->GetY1();
    double px2 = gPad->GetX2();
    double py2 = gPad->GetY2();

    // cout << "px1 " << px1 << " "
    //      << "py1 " << py1 << " "
    //      << "px2 " << px2 << " "
    //      << "py2 " << py2 << " " << endl;

    double ndc_x1, ndc_x2, ndc_y1, ndc_y2;
    if (pad->GetLogx()) {
      ndc_x1 = (log(x1) - px1) / (px2 - px1);
      ndc_x2 = (log(x2) - px1) / (px2 - px1);
    } else {
      ndc_x1 = (x1 - px1) / (px2 - px1);
      ndc_x2 = (x2 - px1) / (px2 - px1);
    }
    if (pad->GetLogy()) {
      ndc_y1 = (log(y1) - py1) / (py2 - py1);
      ndc_y2 = (log(y2) - py1) / (py2 - py1);
    } else {
      ndc_y1 = (y1 - py1) / (py2 - py1);
      ndc_y2 = (y2 - py1) / (py2 - py1);

    }

    // double ndc_x1 = (rx1 - px1) / (px2 - px1);
    // double ndc_x2 = (rx2 - px1) / (px2 - px1);
    // double ndc_y1 = (ry1 - py1) / (py2 - py1);
    // double ndc_y2 = (ry2 - py1) / (py2 - py1);

    return Rectangle(ndc_x1,
                     ndc_y1,
                     ndc_x2,
                     ndc_y2);
    }

  double area() const {
    return (x2-x1)*(y2-y1);
  }
};

std::ostream & operator<<(std::ostream & os, const Rectangle &rc) {
  return os << rc.x1 << ", " << rc.y1 << ", " << rc.x2 << ", " << rc.y2;
}

void placeLegend(TPad *pad, double w, double h, double &x1, double &y1, double &x2, double &y2, bool &inversion) {
  double lm = pad->GetLeftMargin() + 0.04;
  double rm = pad->GetRightMargin() + 0.04;
  double tm = pad->GetTopMargin() + 0.06;
  double bm = pad->GetBottomMargin() + 0.06;

  double leftPos = lm;
  double bottomPos = bm;
  double topPos = 1-tm-h;
  double rightPos = 1-rm-w;
  double xCenterPos = (1-lm-rm)/2 - w/2 + lm;

  std::vector<Rectangle> candidates = {
    // top left
    Rectangle::from_lbwh(leftPos, topPos, w, h),
    // top right
    Rectangle::from_lbwh(rightPos, topPos, w, h),
    // bottom left
    Rectangle::from_lbwh(leftPos, bottomPos, w, h),
    // bottom right
    Rectangle::from_lbwh(rightPos, bottomPos, w, h),
    // top center
    Rectangle::from_lbwh(xCenterPos, topPos, w, h),
    // bottom center
    Rectangle::from_lbwh(xCenterPos, bottomPos, w, h),
  };
  size_t ncandidates = candidates.size();
  std::vector<double> overlap(ncandidates, 0.);

  std::vector<Rectangle> candidates_buffered;
  for (const auto& rc: candidates) {
    candidates_buffered.push_back(rc.expanded(0.06, 0.04));
  }

  bool allow_inverse = true;
  double maxreloverlap = 0.;

  TList *lop = pad->GetListOfPrimitives();
  if (!lop) return;
  TIter next(lop);
  TObject *o = 0;
  while ((o = next())) {
    if (o->InheritsFrom(TH1::Class()) && !o->InheritsFrom("TH2") && !o->InheritsFrom("TH3")) {
      TH1 *hist = (TH1 *)o;
      TAxis *ax = hist->GetXaxis();

      maxreloverlap += 1;

      // nb the bin extents might go outside of the visible area
      // but for the overlap calculation it should be OK.

      double py1 = gPad->GetY1();
      double py2 = gPad->GetY2();
      double biny1 = pad->GetBottomMargin() * (py2-py1) + py1;

      for (int b = 1; b <= ax->GetNbins(); b++) {
        double binx1 = ax->GetBinLowEdge(b);
        double binx2 = ax->GetBinUpEdge(b);
        double biny2 = hist->GetBinContent(b);

        Rectangle column = Rectangle(binx1, biny1, binx2, biny2).histToNDC(pad);
        // // if (b == 1) {
          // cout << "histo coords: " << Rectangle(binx1, biny1, binx2, biny2) << endl;
          // cout << " NDC coords: " << column << endl;
        // // }
        for (size_t i = 0; i < ncandidates; i++) {
          overlap[i] += candidates_buffered[i].overlap(column);
        }
      }
      // break;
    } else if (o->InheritsFrom(TMultiGraph::Class())) {

      TList * grlist = ((TMultiGraph *)o)->GetListOfGraphs();
      TIter nextgraph(grlist);
      TObject * obj;
      while ((obj = nextgraph())) {
        allow_inverse = false;
        maxreloverlap += 1;

        TGraphAsymmErrors *graph = (TGraphAsymmErrors *)obj;
        auto aX = graph->GetX();
        auto aY = graph->GetY();
        auto aEXlow = graph->GetEXlow();
        auto aEXhigh = graph->GetEXhigh();
        auto aEYlow = graph->GetEYlow();
        auto aEYhigh = graph->GetEYhigh();

        double pointWeight = w*h/graph->GetN();
        for (int b = 1; b <= graph->GetN(); b++) {
          double binx1 = aX[b] - aEXlow[b];
          double binx2 = aX[b] + aEXhigh[b];
          double biny1 = aY[b] - aEYlow[b];
          double biny2 = aY[b] + aEYhigh[b];

          Rectangle column = Rectangle(binx1, biny1, binx2, biny2).histToNDC(pad);
          // // if (b == 1) {
            // cout << "histo coords: " << Rectangle(binx1, biny1, binx2, biny2) << endl;
            // cout << " NDC coords: " << column << endl;
          // // }
          for (size_t i = 0; i < ncandidates; i++) {
            // instead of the overlap, give a constant weight per point.
            // hitting many points is worse than covering one very long error bar
            overlap[i] += candidates_buffered[i].overlap(column) > 0 ? pointWeight : 0;
          }
        }

      } // nextgraph

    }
  }

  double boxarea = candidates_buffered[0].area();
  inversion = false;
  size_t mini = 0;
  double minoverlap = overlap[0]/boxarea;
  for (size_t i = 0; i < ncandidates; i++) {
    auto rc = candidates[i];
    cout << "placement " << i << " " << rc << " ";
    double thisoverlap = overlap[i]/boxarea/maxreloverlap;
    cout << "overlap " << thisoverlap << endl;

    if (thisoverlap < minoverlap) {
      mini = i;
      minoverlap = thisoverlap;
      inversion = false;
    }
    if (allow_inverse) {
      double inverseoverlap = (1-thisoverlap) * 10;
      if (inverseoverlap < minoverlap) {
        mini = i;
        minoverlap = inverseoverlap;
        inversion = true;
      }
    }

  }

  Rectangle found = candidates[mini];
  x1 = found.x1;
  x2 = found.x2;
  y1 = found.y1;
  y2 = found.y2;

}

int main(int argc, char **argv)
{
  if (argc != 2) {
    cout << "Error, syntax:" << endl;
    cout << "  " << argv[0] << " submit-dir" << endl;
    cout << "where submit-dir is the directory created by EventLoop"
            "(Run.py), containing hist-*.root ."
         << endl;

    return 1;
  }

  gROOT->SetBatch(true);

  SetAtlasStyle();
  gStyle->SetLegendFont(42); // Helvetica regular
  gStyle->SetLegendBorderSize(0);

  gStyle->SetFrameLineWidth(gStyle->GetFrameLineWidth()*hdScale);
  gStyle->SetHatchesLineWidth(gStyle->GetHatchesLineWidth()*hdScale);
  gStyle->SetHistLineWidth(gStyle->GetHistLineWidth()*hdScale);
  gStyle->SetCanvasBorderSize(gStyle->GetCanvasBorderSize()*hdScale);
  gStyle->SetFrameBorderSize(gStyle->GetFrameBorderSize()*hdScale);
  gStyle->SetPadBorderSize(gStyle->GetPadBorderSize()*hdScale);
  gStyle->SetMarkerSize(gStyle->GetMarkerSize()*hdScale);
  gStyle->SetEndErrorSize(gStyle->GetEndErrorSize()*hdScale);

  TString pattern = TString::Format("%s/hist-*.root", argv[1]);
  TString fname = findFile(pattern);
  if (fname == "") {
    cerr << "Error: No histograms file found in " << argv[1] << endl;
    return 1;
  }
  cout << "Reading histograms from " << fname << endl;

  // argh: https://sft.its.cern.ch/jira/browse/ROOT-7182
  // If you open a file with a relative path, and then change the
  // (operating system) directory, you can't read from it
  // TFile *file = TFile::Open(fname, "read");
  TFile *file = TFile::Open(
      gSystem->ConcatFileName(gSystem->WorkingDirectory(), fname), "read");

  TString comparisonsName = gSystem->BaseName(argv[1]);
  TString outdir = "plots/" + comparisonsName;
  cout << "Making plots in " << outdir << endl;

  gSystem->MakeDirectory("plots");
  gSystem->MakeDirectory(outdir);
  if (!gSystem->ChangeDirectory(outdir)) {
    cout << "Could not open '" << outdir << "' directory." << endl;
    return 1;
  }

  map<string, map<string, TObject *>> cutPlots;
  map<string, TMetaRecord *> cutMeta;

  for (TObject *key : *file->GetListOfKeys()) {
    auto dirname = key->GetName();
    TObject *obj = file->Get(dirname);
    // cout << dirname << ": " << obj->ClassName() << endl;
    // cout << obj->IsFolder() << endl;
    if (!obj->InheritsFrom("TDirectoryFile"))
      continue;
    auto subdir = (TDirectoryFile *)obj;
    gDirectory->cd(subdir->GetPath());
    // todo: ideally read metadata only once in one place
    cutMeta[dirname] = (TMetaRecord *)gDirectory->Get("MetaRecord");
    cout << "path: " << dirname << endl;
    gSystem->MakeDirectory(dirname);
    gSystem->ChangeDirectory(dirname);
    cutPlots[dirname] = makePlots();
    gSystem->ChangeDirectory("..");
  }

  // for the comparison plots
  map<string, string> commonFields;

  auto first = begin(cutMeta);
  auto firstMeta = first->second;

  // todo: ideally, fields at this point would be per-image
  // and not per-cut-folder.

  // go over all fields (defined for the first cut)
  for (const auto &kv : firstMeta->fields) {
    string key, firstValue;
    tie(key, firstValue) = kv;

    bool isCommon = true;

    // go over all cuts
    for (const auto &pair : cutMeta) {
      string cut;
      TMetaRecord *meta;
      tie(cut, meta) = pair;

      if (meta->fields.find(key) == end(meta->fields) ||
          meta->fields[key] != firstValue) {
        isCommon = false;
        break;
      }
    }

    if (isCommon) {
      commonFields[key] = firstValue;
    }
  }

  // make comparisons
  // ----------------

  gStyle->SetOptStat(0);

  gSystem->MakeDirectory("comparisons");
  gSystem->ChangeDirectory("comparisons");
  Int_t colors[] = {
      TColor::GetColor("#e6171e"), // red
      TColor::GetColor("#1687d9"), // blue
      TColor::GetColor("#599900"), // green
      TColor::GetColor("#f5b959"), // orange
      1, 2, 3, 4, 5, 6, 7, 8, 9,
  };

  for (auto var_plot : (begin(cutPlots)->second)) {
    string plotname = var_plot.first;

    double mini = std::numeric_limits<double>::max();
    double maxi = std::numeric_limits<double>::min();
    TH1 *firstHisto{0};
    int index = 0;
    // TCanvas *can = new TCanvas();
    TCanvas *can = new TCanvas("can","can",200,10,800*hdScale,600*hdScale);
    bool isGraph = false;
    TMultiGraph *mg{0};
    for (auto pair : cutPlots) {
      string cut;
      map<string, TObject *> plots;
      tie(cut, plots) = pair;

      TString drawopt = "";

      auto obj = plots[plotname];
      TString title = cutMeta[cut]->fields["description"];
      TGraphAsymmErrors *graph = dynamic_cast<TGraphAsymmErrors *>(obj);
      if (graph) {
        isGraph = true;
        if (index == 0) {
          mg = new TMultiGraph();
        }
        mg->Add(graph);
        graph->SetTitle(title);
        graph->SetLineColor(colors[index]);
        // graph->Draw(drawopt);
        if (index == 0)
          firstHisto = graph->GetHistogram();

        mini = min(mini, graph->GetHistogram()->GetMinimum());
        maxi = max(maxi, graph->GetHistogram()->GetMaximum());
      }

      TH1 *histo = dynamic_cast<TH1 *>(obj);
      if (histo) {
        if (index == 0) {
          drawopt = "hist ";
          firstHisto = histo;
          histo->SetFillColor(colors[index]);
          histo->SetFillStyle(3003);
          // histo->SetFillColorAlpha(colors[index], 0.15);
        } else {
          drawopt = "hist same";
        }
        histo->SetTitle(title);
        histo->SetLineColor(colors[index]);
        histo->Draw(drawopt);
        mini = min(mini, histo->GetMinimum());
        maxi = max(maxi, histo->GetMaximum());
      }

      index++;
    }

    if (isGraph) {
      double range = maxi - mini;
      if (range < 0.2) {
        range = 0.2;
        double mid = (maxi + mini) / 2.;
        maxi = mid + range / 2.;
        mini = mid - range / 2.;
        if (maxi > 1) {
          mini -= (maxi - 1);
          maxi = 1;
        }
      }
    } else {
      double range = maxi - mini;
      if (range < 0.6 * maxi) {
        range = 0.6 * maxi;
        mini = maxi - range;
      }
    }

    if (isGraph && mg) {
      mg->Draw("APEZ");
      mg->GetXaxis()->SetTitle(firstHisto->GetXaxis()->GetTitle());
      mg->GetYaxis()->SetTitle(firstHisto->GetYaxis()->GetTitle());
    }

    cout << plotname << ", " << mini << ", " << maxi << endl;
    bool doLog = false;
    if (plotname == "VertexMatchDistance") {
      doLog = true;
      can->SetLogx(true);
    }
    if (doLog) {
      firstHisto->SetMaximum(maxi * pow(maxi - mini, 0.05));
      if (mini <= 0)
        mini = 0.5;
      firstHisto->SetMinimum(mini);
      can->SetLogy(true);
    } else {
      firstHisto->SetMaximum(maxi + 0.05 * (maxi - mini));
      firstHisto->SetMinimum(mini);
      can->SetLogy(false);
    }
    can->Modified();
    can->Update();
    // if (plotname == "CutflowPairs") {
    //     TLegend *leg = can->BuildLegend(0.2, 0.2, 0.55, 0.45);
    //     leg->SetFillStyle(0);
    // } else {
    //     TLegend *leg = can->BuildLegend(0.6, 0.2, 0.9, 0.4);
    //     leg->SetFillStyle(0);
    // }
    double x1 = 0;
    double y1 = 0;
    double x2 = 0;
    double y2 = 0;
    bool inversion = false;
    placeLegend(can, 0.35, 0.2, x1, y1, x2, y2, inversion);
    TLegend *leg = can->BuildLegend(x1, y1, x2, y2);
    if (!inversion)
      leg->SetFillStyle(0);
    // if (inversion) {
    //   leg->SetFillColor(kCyan-10);
    // }
    // leg->SetFillStyle(0);


    // // add a line at y==1
    // if (isGraph) {
    //     TLine *line = new TLine();
    //     line->SetLineStyle(kDashed);
    //     line->SetLineWidth(1);
    //     auto xa = firstHisto->GetXaxis();
    //     line->DrawLine(xa->GetXmin(), 1, xa->GetXmax(), 1);
    // }

    string outname = plotname + ".png";
    //string outname = plotname + ".C";
    can->SaveAs(outname.c_str());

    map<string, string> fields;
    fields["variable"] = plotname;
    fields["plotCreated"] = getTimeStr();

    fields["ymin"] = TString::Format("%g", mini);
    fields["ymax"] = TString::Format("%g", maxi);
    for (const auto &pair : commonFields) {
      fields[pair.first] = pair.second;
    }
    string sAllCuts = "";
    for (auto pair : cutPlots) {
      if (sAllCuts != "")
        sAllCuts += ", ";
      sAllCuts += pair.first;
    }
    fields["comparing"] = sAllCuts;
    addPNGFields(outname.c_str(), fields);
  }
}
