#ifndef TVATest_TVATester_H
#define TVATest_TVATester_H

// EventLoop
#include "EventLoop/Algorithm.h"

// xAOD
#include "xAODRootAccess/TEvent.h"
#include "xAODTruth/TruthVertex.h"

// Tools
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"
#include "InDetTruthVertexValidation/InDetVertexTruthMatchTool.h"
#include "TrackVertexAssociationTool/ITrackVertexAssociationTool.h"

// Root
#include "TH1.h"

enum CutflowPairs {
  All,
  Truth,
  Correct,
  Reco,
  Fake,
  Missed,
  NoTruth,
  COUNT,
};

enum CutflowVertices {
    cvAll,         ///< All reco vertices
    cvMatched,     ///< Matched by the selected match method
    cvCorrectType, ///< Correct VertexMatchType, e.g. not FAKE or DUMMY
    cvCOUNT,
};

class TVATester : public EL::Algorithm
{
public:
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.

  // float cutValue;
  std::string associationMethod;
  std::string directoryName;
  std::string description;
  bool doTrackCut;
  std::string trackCutLevel;
  double trackMinPt;
  /// PDG id of the tracks to select.  No cut if <= 0.
  int trackAbsPdgId;
  /// How to match reco to truth vertices: smart (default) or nearest
  std::string matchMethod;

  // this is a standard constructor
  TVATester();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob(EL::Job &job);
  virtual EL::StatusCode fileExecute();
  virtual EL::StatusCode histInitialize();
  virtual EL::StatusCode changeInput(bool firstFile);
  virtual EL::StatusCode initialize();
  virtual EL::StatusCode execute();
  virtual EL::StatusCode postExecute();
  virtual EL::StatusCode finalize();
  virtual EL::StatusCode histFinalize();

private:
  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
  CP::ITrackVertexAssociationTool *m_tvatool; //!
  xAOD::TEvent *m_event;                      //!
  InDetVertexTruthMatchTool *m_matchTool;     //!
  InDet::InDetTrackSelectionTool *m_trackSel; //!

  TH1 *m_hPtT;  //!
  TH1 *m_hPtNoT;  //!
  TH1 *m_hPtNoTR;  //!
  TH1 *m_hPtTR; //!
  TH1 *m_hPtR; //!

  // TH1 *m_hIPtT; //!
  // THNo1 *m_hIPtT; //!
  // THNo1R *m_hIPtT; //!
  // TH1 *m_hIPtTR; //!

  TH1 *m_hPtVariableT; //!
  TH1 *m_hPtVariableNoT; //!
  TH1 *m_hPtVariableNoTR; //!
  TH1 *m_hPtVariableTR; //!
  TH1 *m_hPtVariableR; //!

  TH1 *m_hphiT; //!
  TH1 *m_hphiNoT; //!
  TH1 *m_hphiNoTR; //!
  TH1 *m_hphiTR; //!
  TH1 *m_hphiR; //!

  TH1 *m_hetaT; //!
  TH1 *m_hetaNoT; //!
  TH1 *m_hetaNoTR; //!
  TH1 *m_hetaTR; //!
  TH1 *m_hetaR; //!

  TH1 *m_heT; //!
  TH1 *m_heNoT; //!
  TH1 *m_heNoTR; //!
  TH1 *m_heTR; //!
  TH1 *m_heR; //!

  TH1 *m_hmT; //!
  TH1 *m_hmNoT; //!
  TH1 *m_hmNoTR; //!
  TH1 *m_hmTR; //!
  TH1 *m_hmR; //!

  TH1 *m_hd0T;  //!
  TH1 *m_hd0NoT;  //!
  TH1 *m_hd0NoTR;  //!
  TH1 *m_hd0TR; //!
  TH1 *m_hd0R; //!
  TH1 *m_hd0sigT;  //!
  TH1 *m_hd0sigNoT;  //!
  TH1 *m_hd0sigNoTR;  //!
  TH1 *m_hd0sigTR; //!
  TH1 *m_hd0sigR; //!

  TH1 *m_hdzSinThetaT;  //!
  TH1 *m_hdzSinThetaNoT;  //!
  TH1 *m_hdzSinThetaNoTR;  //!
  TH1 *m_hdzSinThetaTR; //!
  TH1 *m_hdzSinThetaR; //!

  TH1 *m_hchi2T;  //!
  TH1 *m_hchi2NoT;  //!
  TH1 *m_hchi2NoTR;  //!
  TH1 *m_hchi2TR; //!
  TH1 *m_hchi2R; //!

  TH1 *m_hchi2NDFT;  //!
  TH1 *m_hchi2NDFNoT;  //!
  TH1 *m_hchi2NDFNoTR;  //!
  TH1 *m_hchi2NDFTR; //!
  TH1 *m_hchi2NDFR; //!

  TH1 *m_hnTRTHitsT;  //!
  TH1 *m_hnTRTHitsNoT;  //!
  TH1 *m_hnTRTHitsNoTR;  //!
  TH1 *m_hnTRTHitsTR; //!
  TH1 *m_hnTRTHitsR; //!
  TH1 *m_hnSCTHitsT;  //!
  TH1 *m_hnSCTHitsNoT;  //!
  TH1 *m_hnSCTHitsNoTR;  //!
  TH1 *m_hnSCTHitsTR; //!
  TH1 *m_hnSCTHitsR; //!
  TH1 *m_hnPixelHitsT;  //!
  TH1 *m_hnPixelHitsNoT;  //!
  TH1 *m_hnPixelHitsNoTR;  //!
  TH1 *m_hnPixelHitsTR; //!
  TH1 *m_hnPixelHitsR; //!

  TH1 *m_hmuActualT;  //!
  TH1 *m_hmuActualNoT;  //!
  TH1 *m_hmuActualNoTR;  //!
  TH1 *m_hmuActualTR; //!
  TH1 *m_hmuActualR; //!

  TH1 *m_hVertexMatchWeightT;  //!
  TH1 *m_hVertexMatchWeightNoT;  //!
  TH1 *m_hVertexMatchWeightNoTR;  //!
  TH1 *m_hVertexMatchWeightTR; //!
  TH1 *m_hVertexMatchWeightR; //!

  TH1 *m_hTruthMatchProbabilityT;  //!
  TH1 *m_hTruthMatchProbabilityNoT;  //!
  TH1 *m_hTruthMatchProbabilityNoTR;  //!
  TH1 *m_hTruthMatchProbabilityTR; //!
  TH1 *m_hTruthMatchProbabilityR; //!

  TH1 *m_hNTracks;           //!
  TH1 *m_hNTruthMatched;     //!
  TH1 *m_hNPassTrackSel;     //!
  TH1 *m_hNCorrectParticle;  //!
  TH1 *m_hNTrackVertexPairs; //!
  TH1 *m_hNTruthAssoc;       //!
  TH1 *m_hNRecoAssoc;        //!
  TH1 *m_hNMissed;           //!
  TH1 *m_hNFake;             //!
  TH1 *m_hNCorrect;          //!
  TH1 *m_hNVertices;         //!
  TH1 *m_hCutflowTracks;     //!
  TH1 *m_hCutflowVertices;   //!
  TH1 *m_hCutflowPairs;      //!

  TH1 *m_hVertexMatchDistance; //!
  TH1 *m_hVertexMatchTypeAll;  //!
  TH1 *m_hVertexMatchType;     //!
  TH1 *m_hTruthVertexBarcode;  //!

  /// Returns the best guess of a truth vertex for a given reco vertex.
  const xAOD::TruthVertex *getClosestTruthVertex(const xAOD::Vertex *rv) const;

  void dump(const char *location, const xAOD::TruthVertex *vx,
            const xAOD::Vertex *pv, uint nparents = 0, uint indent = 0) const;
  template <typename... T>
  void myInfo(const char *location, uint indent, const char *msgfmt,
              T... args) const;

  // this is needed to distribute the algorithm to the workers
  ClassDef(TVATester, 1);
};

#endif
