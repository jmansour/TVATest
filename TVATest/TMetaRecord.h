
#ifndef TVATEST_TMETARECORD_H
#define TVATEST_TMETARECORD_H

#include "TObject.h"
#include "TNamed.h"

#include <map>
#include <string>

class TMetaRecord: public TNamed {

public:
  std::map<std::string, std::string> fields;

  using TNamed::TNamed;

  ClassDef(TMetaRecord, 1)
};
#endif//TVATEST_TMETARECORD_H
// vim: expandtab tabstop=8 shiftwidth=2 softtabstop=2
