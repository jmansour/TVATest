TVATest
=======
This tool is supposed to take a track-vertex association method (e.g. in form of
a [CP::ITrackVertexAssociationTool][1], and evaluate it using certain figures of
merit, e.g. efficiency, fake association rate, etc..

[1]: http://atlas-computing.web.cern.ch/atlas-computing/links/nightlyDocDirectory/TrackVertexAssociationTool/html/annotated.html

Getting started
---------------

    # First time
    setupATLAS
    mkdir workdir
    cd workdir
    rcSetup Base,2.3.32
    git clone ssh://git@gitlab.cern.ch:7999/jmansour/TVATest.git
    rc checkout_pkg atlasoff/InnerDetector/InDetRecTools/TrackVertexAssociationTool/trunk TrackVertexAssociationTool
    rc checkout_pkg atlasoff/InnerDetector/InDetValidation/InDetTruthVertexValidation/trunk InDetTruthVertexValidation
    
    # Later
    source rcSetup.sh
    
    # Build
    rc find_packages
    rc compile

See the TWiki for the [latest recommended release][2] of RootCore.

[2]: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisRelease#AnalysisBase

Running
-------
To run the tester, just do

    python TVATest/scripts/Run.py 
    
You should edit Run.py to specify what data you want to run on. It creates a
directory (e.g. `compare_tracksel`) that you have to delete before you run again:
    
    rm -rf compare_tracksel; python TVATest/scripts/Run.py

There are different blocks in Run.py that you can comment out, that compare different things:
* Track selections (Tight Primary, Loose, None) -> `compare_tracksel`
* TVA methods (Tight, Loose, Electron, Muon) -> `compare_methods`
* Truth matching strategy ("smart" with InDetVertexTruthMatchTool or "nearest", just taking the nearest truth vertex) -> `compare_matching`

### Making efficiency plots
There is an experimental program to make a bunch of plots, `ratioPlots`.  After compiling the package, call it with the submit dir as a parameter.  It searches for `hist-*.root` in that directory:

    ratioPlots compare_tracksel

It creates directories like `plots/compare_tracksel`, `plots/compare_tracksel/loose`, `plots/compare_tracksel/tight`, etc. for the different configurations used, and also comparisons in `plots/compare_tracksel/comparisons`.

### Making HTML plot listing
To make a nice plot listing that you can view in your browser, you can use `makeindexes.py`.  This is also a bit experimental and depends on the jinja2 template engine.  One way to use it is:

    virtualenv ~/virtualenvs/ttvat
    ~/virtualenvs/ttvat/bin/pip install jinja2
    ~/virtualenvs/ttvat/bin/python makeindexes.py
    
    # you might want to copy the files somewhere you can view them over the net:
    cp -r plots -t ~/www/private

The script makes `index.html` files in all subdirectories of `plots`.  Currently, the path is hardcoded, but it will be made an argument.

## TVATester properties
The TVATester class is configurable by a couple of properties.  This list might not be up to date, so check the code.

* directoryName (string): Creates a sub-directory in the output .root file, and stores the histograms in there.  Useful to run multiple TVATesters from a single script.
* trackCutLevel (string): Sets the cut level for the track selection, e.g. TightPrimary or Loose.
* trackMinPt (float): Sets the minimul pT cut for tracks, in MeV.
* matchMethod (string): How to find truth vertices for reco vertices?
  * `smart`: Use the InDetVertexTruthMatchTool
  * `nearest`: Use the closest truth vertex (with the correct barcode)
* associationMethod (string): Which TrackVertexAssociationTool to use?  `Tight`, `Loose`, `Electron` or `Muon`.
  * This setting is the reason I'm thinking about switching to Athena.  In Athena you could completely create the tools (with custom parameters) in the jobOptions file and pass them to the tester.  The tester would only take a handle to the tool.  In RootCore we need to make every variation into a parameter of TVATester.

## Metadata
This is part of a little experiment.  TVATester writes some metadata about its settings to the output .root file, e.g. the track cut settings.  For this, it creates a TMetaRecord object and puts it in the output tree.  The TMetaRecord object just holds a list of keys and values (currently only strings).  When creating .png plots (by the ratioPlots program), the meta values are read, and then put into the .png files.  The idea is that if you encounter a .png file after a while, you can find out exactly where it comes from and what it shows.  Finally, the script that creates a directory listing (makeindexes.py) reads this info from the .png files and puts it into the listing.